<?php
/*
Plugin Name: JS Menu Order Sorter
Plugin URI: http://joshstopper.com.au
Description: Sort posts on the admin screen by drag and dropping them to change their menu order
Author: Josh Stopper
Version: 1.0
Author URI: http://joshstopper.com.au
*/
class JS_Menu_Order_Sorter{
	private $name = 'js-menu-order-sorter';
	private $name_clean = 'Menu Order Sorter';
	private $version = 0.1;
	private $options_name = "js-menu-order-sorter_options";
	private $options;

	public function __construct(){

		if ( is_admin() ){
			// Register a new menu item
			add_action( 'admin_menu', array($this,'admin_menu'));
			
			// Create some new settings
			add_action( 'admin_init', array($this,'admin_init'));	

			// Add the ajax action
			// Has to be initiated on plugin initialisation as it is too late in other actions
			add_action('wp_ajax_js_update_posts_order', array($this, 'update_posts_order'));		

		} else {
			// non-admin enqueues, actions, and filters
			add_action( 'pre_get_posts', array($this,'pre_get_posts'));
		}
	}

	// ----------------------------------------------------------------------
	// Create the admin page which makes it appear in the sidebar
	// ----------------------------------------------------------------------
	public function admin_menu() {
		add_options_page(
			$this->name_clean,
			$this->name_clean,
			'manage_options',
			$this->name,
			array($this,'show_page_content')
		);

	}

	// ----------------------------------------------------------------------
	// Load all the things if we are on the admin page
	// ----------------------------------------------------------------------
	public function admin_init() {
		global $pagenow, $typenow;
		$subpage = '';
		if(isset($_GET['page'])){
			$subpage = $_GET['page'];
		}

		// ----------------------------------------------------------------------
		// Always Load this stuff
		// ----------------------------------------------------------------------
		
		// Registers an area to store our plugin data
		// Additionally specifies how to validate the data when saving
		register_setting( $this->options_name, $this->options_name, array($this, 'plugin_options_validate'));
		
		// Create an area for the settings to be displayed on the correct page
		// This creates the menu item in the sidebar
		add_settings_section('plugin_main', '', array($this,'plugin_section_text'), $this->options_name);

		// ----------------------------------------------------------------------
		// Do this stuff on the options page
		// ----------------------------------------------------------------------
		if($pagenow==='options-general.php' && $subpage===$this->name){
			
			// Create the markup required for the edit screen
			add_settings_field('plugin_text_string', '', array($this,'plugin_setting_string'), $this->options_name, 'plugin_main');

			// Load styles and scripts for the admin
			// Should probably do this in two calls
			add_action('admin_enqueue_scripts',			array($this, 'enqueue_assets'));
		}

		// ----------------------------------------------------------------------
		// Do this stuff on the posts page
		// ----------------------------------------------------------------------
		if($pagenow==='edit.php'){
			$this->options = get_option($this->options_name);

			if(array_key_exists($typenow, $this->options)){

				if(isset($this->options[$typenow]['sortable']) && $this->options[$typenow]['sortable']==='on'){

					// Create the column
					add_filter( 'manage_'.$typenow.'_posts_columns', 			array($this, 'create_column' ));
					// Give it some data
					add_action( 'manage_'.$typenow.'_posts_custom_column' , 	array($this, 'add_column_content'), 10, 2 );
					// Make it sortable
					add_filter( 'manage_edit-'.$typenow.'_sortable_columns', 	array($this, 'make_column_sortable'));
					// Change the default order of posts on the admin screens
					add_filter('pre_get_posts', 								array($this, 'set_post_order_in_admin') );
					// Load some styles
					add_action('admin_enqueue_scripts',			array($this, 'enqueue_assets'));
				}
			}
		}
		
	}

	// ----------------------------------------------------------------------
	// Create the page wrapper html for the admin page
	// ----------------------------------------------------------------------
	public function show_page_content() {
		?>
		<div class="wrap">
		    <h2><?php echo $this->name_clean; ?></h2>
	    	<form action="options.php" method="post" class="js_menu_order_sorter__form">

	    		<?php settings_fields($this->options_name); ?>
	    		<?php do_settings_sections($this->options_name); ?>
	    		
	    		<input name="Submit" type="submit" value="<?php esc_attr_e('Save'); ?>" />

	    	</form>
		</div>
		<?php
	}

	// ----------------------------------------------------------------------
	// Create all the form fields
	// ----------------------------------------------------------------------
	public function plugin_setting_string() {
		$options = get_option($this->options_name);

		// ----------------------------------------------------------------------
		// Create the table headers
		// ----------------------------------------------------------------------
		?>
		<div class="js_menu_order_sorter__table">
			<div class="js_menu_order_sorter__tr">
				<div class="js_menu_order_sorter__th">
					Post Type
				</div>
				<div class="js_menu_order_sorter__th">
					Sortable
				</div>
				<?php /*
				<div class="js_menu_order_sorter__th">
					Admin Order
				</div>
				*/ ?>
				<div class="js_menu_order_sorter__th">
					Template Order
				</div>
				<div class="js_menu_order_sorter__th">
					Show All
				</div>
			</div>
		<?php

		$post_types = get_post_types();
		?>
		
		<?php
		foreach ($post_types as $key => $post_type) {
			if(post_type_supports($post_type, 'page-attributes')){
				
				// Determine if the checkbox was checked
				// Additionally, if it wasnt checked, there is no entry, so lets add one incase to not cause errors
				if(isset($options[$post_type]['sortable']) && $options[$post_type]['sortable']==='on'){
					$options[$post_type]['sortable'] = 'checked';
				}else{
					$options[$post_type]['sortable'] = '';
				}
				if(isset($options[$post_type]['show_all']) && $options[$post_type]['show_all']==='on'){
					$options[$post_type]['show_all'] = 'checked';
				}else{
					$options[$post_type]['show_all'] = '';
				}

				?>
				
				<div class="js_menu_order_sorter__tr">
					<div class="js_menu_order_sorter__td text-capitalize">
						<?php echo $post_type; ?>
					</div>
					
					<div class="js_menu_order_sorter__td">
						<input type="checkbox" name="<?php echo $this->options_name; ?>[<?php echo $post_type;?>][sortable]" <?php echo $options[$post_type]['sortable']; ?>>
					</div>
					<?php /*
					<div class="js_menu_order_sorter__td">
						<select name="<?php echo $this->options_name; ?>[<?php echo $post_type;?>][admin_order]">
							<option value="">Default</option>
							<option value="menu_order" <?php if($options[$post_type]['admin_order']==='menu_order'){ ?>selected<?php } ?>>Menu Order</option>
						</select>
					</div>
					*/ ?>
					<div class="js_menu_order_sorter__td">
						<select name="<?php echo $this->options_name; ?>[<?php echo $post_type;?>][template_order]">
							<option value="">Default</option>
							<option value="ASC" <?php if($options[$post_type]['template_order']==='ASC'){ ?>selected<?php } ?>>Menu Order Ascending</option>
							<option value="DESC" <?php if($options[$post_type]['template_order']==='DESC'){ ?>selected<?php } ?>>Menu Order Descending</option>
						</select>
					</div>
					<div class="js_menu_order_sorter__td">
						<input type="checkbox" name="<?php echo $this->options_name; ?>[<?php echo $post_type;?>][show_all]" <?php echo $options[$post_type]['show_all']; ?>>
					</div>
				</div>
				
			<?php }
		} ?>
		</div>
	<?php }

	public function plugin_section_text() {
		echo '<p>Use the settings here to control the display of post types in the admin area and the frontend.</p>';
	}

	// validate our options
	public function plugin_options_validate($input) {

		return $input;

		// $options = get_option('plugin_options');
		// $options = get_option($this->options_name);
		// $options['text_string'] = trim($input['text_string']);
		// if(!preg_match('/^[a-z0-9]{32}$/i', $options['text_string'])) {
		// 	$options['text_string'] = '';
		// }
		// return $options;
	}

	function set_post_order_in_admin( $wp_query ) {

		$options = get_option($this->options_name);
		
		if(!isset($wp_query->query['order'])){

			$wp_query->set( 'orderby', 'menu_order' );
			$wp_query->set( 'order', 'ASC' );
		}
	}

	public function create_column($columns){
		$columns['menu_order'] = "Order";
		return $columns;
	}

	public function make_column_sortable($columns){
		$columns['menu_order'] = 'menu_order';
		return $columns;
	}

	public function add_column_content($column, $post_id){

		switch ( $column ) {
			case 'menu_order':
				$thispost = get_post($post_id);
				echo $thispost->menu_order;
				break;
			default:
				break;
		}
	}

	public function update_posts_order(){

		ob_clean();
		parse_str($_POST['order'], $data);

		if(!empty($data['post'])){
		    foreach ($data['post'] as $order => $id) {
		        $my_post = get_post($id);
		        $my_post->menu_order = $order;
		        wp_update_post( $my_post );
		    }
		}

		echo true;
		// this is required to terminate immediately and return a proper response
		wp_die();
	}

	public function enqueue_assets(){
		$this->enqueue_styles();
		$this->enqueue_scripts();
	}

	private function enqueue_styles(){
		// Load our styles which make it look like a draggable interface
		// Additionally, it loads the styling for the options page
			// should seperate these styles
		wp_enqueue_style('js-menu-order-sorter-css', plugins_url( "css/styles.css", __FILE__ ), array(), $this->version, 'all');
	}

	private function enqueue_scripts(){

		// Lets put some custom information in to the dom so we can pick get the information we need to post data
		wp_localize_script( 'ajax-script', 'ajax_object', array( 
			'ajax_url' => admin_url( 'admin-ajax.php' )
		));

		// Lets get jQueryUI in here so we can enable drag and drop
		wp_enqueue_script('jquery-ui-sortable');

		// Lets load the custom javascript we need
		wp_enqueue_script( 'js-menu-order-sorter-js', plugins_url( "js/scripts.js", __FILE__ ), array('jquery'), $this->version, true);
	}

	// --------------------------------------------------------
	// Modifies the query on the product archive and product taxonomy page to show all products
	// --------------------------------------------------------
	public function pre_get_posts( $query ) {
		
		if($query->is_post_type_archive==1){

			$options = get_option($this->options_name);

			if(isset($options[$query->query['post_type']]['template_order']) && $options[$query->query['post_type']]['template_order']!=""){
		       $query->set('orderby',	'menu_order');
		       $query->set('order',		$options[$query->query['post_type']]['template_order'] );
			}

			if(isset($options[$query->query['post_type']]['show_all']) && $options[$query->query['post_type']]['show_all']!=""){
				$query->set('posts_per_page',   -1);
			}	
		}
	}
}

new JS_Menu_Order_Sorter();