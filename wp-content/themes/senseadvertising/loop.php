<?php if(have_posts()) { ?>
    <?php while(have_posts()) { the_post(); ?>
        <div class="col-xs-12 col-sm-6 col-md-4 js_shuffle_item">
            <?php get_template_part('content', get_post_format()); ?>
        </div>
    <?php } ?>
<?php } ?>