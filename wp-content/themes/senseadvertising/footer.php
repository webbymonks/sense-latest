    </div>

    <footer class="footer">

        <div class="footer__bg"></div>

        <!-- <div class="footer__half_bg"></div> -->

        <div class="footer__inner">

            <div class="footer__sub">
                <div class="container-fluid">
                    <div class="row">
                        <div class="hide_br_small_screens col-sm-8 footer__contact">
                            <h4><?php echo get_field('footer_title','option'); ?></h4>
                            <?php echo get_field('footer_content','option'); ?>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-3 col-lg-offset-1 footer__social">
                            <h4>Let's be friends</h4>
                            <ul class="social">
                                <li>
                                    <a class="btn--social socicon-facebook" href="https://www.facebook.com/senseagency" target="_blank">
                                        <span class="hover_line hover_line--small hover_line--white"></span>
                                    </a>
                                </li>
                                <li>
                                    <a class="btn--social socicon-twitter" href="https://twitter.com/sense_agency" target="_blank">
                                        <span class="hover_line hover_line--small hover_line--white"></span>
                                    </a>
                                </li>
                                <li class="alt">
                                    <a class="btn--social socicon-instagram" href="https://www.instagram.com/sense.agency/" target="_blank">
                                        <span class="hover_line hover_line--small hover_line--white"></span>
                                    </a>
                                </li>
                                <li class="alt">
                                    <a class="btn--social socicon-linkedin" href="http://www.linkedin.com/company/433075" target="_blank">
                                        <span class="hover_line hover_line--small hover_line--white"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <span class="footer__arrow"></span>

            </div>

            <div class="footer__bottom">
                <div class="footer__logo">
                    <a href="<?php bloginfo('url'); ?>">
                        <?php //echo file_get_contents(get_template_directory().'/img/logo.svg'); ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.svg" alt="Sense Advertising" />
                    </a>
                </div>
                <?php wp_nav_menu( array(
                    'menu'              => 'footer',
                    'theme_location'    => 'footer',
                    'depth'             => 0,
                    'container'         => '',
                    'container_class'   => '',
                    'menu_class'        => 'footer__menu',
                    'fallback_cb'       => false
                )); ?>
            </div>

        </div>

    </footer>
    <?php /*
    <?php if(!isset($_SESSION['digital_overlay'])){ ?>
        <?php $_SESSION['digital_overlay']='hide'; ?>
        <div class="digital_overlay">
            <div class="digital_overlay__inner">
                <div class="digital_overlay__header">
                    <button class="digital_overlay__close"></button>
                </div>
                <div class="digital_overlay__top no_last_margin">
                    <p class="digital_overlay__heading">
                        Do digital right in 2017.
                    </p>
                    <p>
                        Complete our 5-minute survey to receive a FREE report on emergent trends and opportunities in the digital ad and marketing space, sent to you at the end of January. It will be the most up-to-the-minute insight into all things digital in the country.
                    </p>
                </div>
                <div class="digital_overlay__middle no_last_margin">
                    <p>
                        PLUS!
                    </p>
                    <p>
                        Complete the survey and you could win 20hrs of strategic digital planning for your business with Damien Cheney, Digital Director at Sense.
                    </p>
                </div>
                <div class="digital_overlay__bottom no_last_margin">
                <p class="digital_overlay__bottom__heading">
                        WHO WE ARE
                    </p>
                    <p>
                        Sense is a multidisciplinary creative agency responsible for award-winning integrated campaigns for clients all over Australia. Say hello@sense.com.au for more info.
                    </p>
                </div>
                <div class="digital_overlay__footer no_last_margin">
                    <p>
                        <a href="mailto:hello@sense.com.au">Say hello@sense.com.au for more info.</a>
                    </p>
                </div>
            </div>
            <script type="text/javascript">
                jQuery(document).ready(function($){
                    $(".digital_overlay__close").click(function(){
                        $(".digital_overlay").addClass('hide');
                    });

                    $('.digital_overlay').on('click', function(event) {
                        if (!$(event.target).closest('.digital_overlay__inner').length) {
                            $(this).addClass('hide');
                            // $("html,body").removeClass('no_scroll');
                        }
                    });
                });


            </script>
        </div>
    <?php } ?>
    */ ?>

    <div class="tracking_code_fix">
    <script type="text/javascript">
    var _ss = _ss || [];
    _ss.push(['_setDomain', 'https://koi-3QAWCD2VD4.marketingautomation.services/net']);
    _ss.push(['_setAccount', 'KOI-3QEFSEEW5K']);
    _ss.push(['_trackPageView']);
    (function() {
        var ss = document.createElement('script');
        ss.type = 'text/javascript'; ss.async = true;

        ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QAWCD2VD4.marketingautomation.services/client/ss.js?ver=1.1.1';
        var scr = document.getElementsByTagName('script')[0];
        scr.parentNode.insertBefore(ss, scr);
    })();
    </script>
    <script type="text/javascript">
        var __ss_noform = __ss_noform || [];
        __ss_noform.push(['baseURI', 'https://app-3QAWCD2VD4.marketingautomation.services/webforms/receivePostback/MzYwNzM3MjAzAwA/']);
        __ss_noform.push(['endpoint', '8a718be5-a2a4-4ebb-9cf6-880acd313297']);
    </script>
    <script type="text/javascript" src="https://koi-3QAWCD2VD4.marketingautomation.services/client/noform.js?ver=1.24" ></script>
    </div>
    <?php wp_footer(); ?>
</body>

</html>
