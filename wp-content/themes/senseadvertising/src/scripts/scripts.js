//To include one of these libraries, remove two of the slashes from the beginning

////@codekit-prepend "../../bower_components/slick-carousel/slick/slick.min.js"
////@codekit-prepend "../../bower_components/bootstrap/js/transition.js"
////@codekit-prepend "../../bower_components/bootstrap/js/modal.js"
////@codekit-prepend "../../bower_components/bootstrap/js/collapse.js"
////@codekit-prepend "../../bower_components/photoswipe/dist/photoswipe.min.js"
////@codekit-prepend "../../bower_components/photoswipe/dist/photoswipe-ui-default.min.js"

//@codekit-prepend "../../bower_components/wow/dist/wow.js"
//@codekit-prepend "../../bower_components/imagesloaded/imagesloaded.pkgd.min.js"
//@codekit-prepend "../../bower_components/bootstrap/js/dist/util.js"
//@codekit-prepend "../../bower_components/bootstrap/js/dist/tab.js"
//@codekit-prepend "../../bower_components/shufflejs/dist/jquery.shuffle.modernizr.min.js"

/*! modernizr 3.3.1 (Custom Build) | MIT *
 * http://modernizr.com/download/?-objectfit-setclasses !*/

//Modernizr - object-fit

jQuery(document).ready(function ($) {

    ! function (e, n, t) {
        function r(e, n) {
            return typeof e === n
        }

        function o() {
            var e, n, t, o, i, s, a;
            for (var f in g)
                if (g.hasOwnProperty(f)) {
                    if (e = [], n = g[f], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length))
                        for (t = 0; t < n.options.aliases.length; t++) e.push(n.options.aliases[t].toLowerCase());
                    for (o = r(n.fn, "function") ? n.fn() : n.fn, i = 0; i < e.length; i++) s = e[i], a = s.split("."), 1 === a.length ? Modernizr[a[0]] = o : (!Modernizr[a[0]] || Modernizr[a[0]] instanceof Boolean || (Modernizr[a[0]] = new Boolean(Modernizr[a[0]])), Modernizr[a[0]][a[1]] = o), y.push((o ? "" : "no-") + a.join("-"))
                }
        }

        function i(e) {
            var n = _.className,
                t = Modernizr._config.classPrefix || "";
            if (w && (n = n.baseVal), Modernizr._config.enableJSClass) {
                var r = new RegExp("(^|\\s)" + t + "no-js(\\s|$)");
                n = n.replace(r, "$1" + t + "js$2")
            }
            Modernizr._config.enableClasses && (n += " " + t + e.join(" " + t), w ? _.className.baseVal = n : _.className = n)
        }

        function s(e) {
            return e.replace(/([a-z])-([a-z])/g, function (e, n, t) {
                return n + t.toUpperCase()
            }).replace(/^-/, "")
        }

        function a() {
            return "function" != typeof n.createElement ? n.createElement(arguments[0]) : w ? n.createElementNS.call(n, "http://www.w3.org/2000/svg", arguments[0]) : n.createElement.apply(n, arguments)
        }

        function f(e, n) {
            return !!~("" + e).indexOf(n)
        }

        function l(e, n) {
            return function () {
                return e.apply(n, arguments)
            }
        }

        function u(e, n, t) {
            var o;
            for (var i in e)
                if (e[i] in n) return t === !1 ? e[i] : (o = n[e[i]], r(o, "function") ? l(o, t || n) : o);
            return !1
        }

        function p(e) {
            return e.replace(/([A-Z])/g, function (e, n) {
                return "-" + n.toLowerCase()
            }).replace(/^ms-/, "-ms-")
        }

        function d() {
            var e = n.body;
            return e || (e = a(w ? "svg" : "body"), e.fake = !0), e
        }

        function c(e, t, r, o) {
            var i, s, f, l, u = "modernizr",
                p = a("div"),
                c = d();
            if (parseInt(r, 10))
                for (; r--;) f = a("div"), f.id = o ? o[r] : u + (r + 1), p.appendChild(f);
            return i = a("style"), i.type = "text/css", i.id = "s" + u, (c.fake ? c : p).appendChild(i), c.appendChild(p), i.styleSheet ? i.styleSheet.cssText = e : i.appendChild(n.createTextNode(e)), p.id = u, c.fake && (c.style.background = "", c.style.overflow = "hidden", l = _.style.overflow, _.style.overflow = "hidden", _.appendChild(c)), s = t(p, e), c.fake ? (c.parentNode.removeChild(c), _.style.overflow = l, _.offsetHeight) : p.parentNode.removeChild(p), !!s
        }

        function m(n, r) {
            var o = n.length;
            if ("CSS" in e && "supports" in e.CSS) {
                for (; o--;)
                    if (e.CSS.supports(p(n[o]), r)) return !0;
                return !1
            }
            if ("CSSSupportsRule" in e) {
                for (var i = []; o--;) i.push("(" + p(n[o]) + ":" + r + ")");
                return i = i.join(" or "), c("@supports (" + i + ") { #modernizr { position: absolute; } }", function (e) {
                    return "absolute" == getComputedStyle(e, null).position
                })
            }
            return t
        }

        function v(e, n, o, i) {
            function l() {
                p && (delete z.style, delete z.modElem)
            }
            if (i = r(i, "undefined") ? !1 : i, !r(o, "undefined")) {
                var u = m(e, o);
                if (!r(u, "undefined")) return u
            }
            for (var p, d, c, v, h, y = ["modernizr", "tspan"]; !z.style;) p = !0, z.modElem = a(y.shift()), z.style = z.modElem.style;
            for (c = e.length, d = 0; c > d; d++)
                if (v = e[d], h = z.style[v], f(v, "-") && (v = s(v)), z.style[v] !== t) {
                    if (i || r(o, "undefined")) return l(), "pfx" == n ? v : !0;
                    try {
                        z.style[v] = o
                    } catch (g) {}
                    if (z.style[v] != h) return l(), "pfx" == n ? v : !0
                }
            return l(), !1
        }

        function h(e, n, t, o, i) {
            var s = e.charAt(0).toUpperCase() + e.slice(1),
                a = (e + " " + S.join(s + " ") + s).split(" ");
            return r(n, "string") || r(n, "undefined") ? v(a, n, o, i) : (a = (e + " " + E.join(s + " ") + s).split(" "), u(a, n, t))
        }
        var y = [],
            g = [],
            C = {
                _version: "3.3.1",
                _config: {
                    classPrefix: "",
                    enableClasses: !0,
                    enableJSClass: !0,
                    usePrefixes: !0
                },
                _q: [],
                on: function (e, n) {
                    var t = this;
                    setTimeout(function () {
                        n(t[e])
                    }, 0)
                },
                addTest: function (e, n, t) {
                    g.push({
                        name: e,
                        fn: n,
                        options: t
                    })
                },
                addAsyncTest: function (e) {
                    g.push({
                        name: null,
                        fn: e
                    })
                }
            },
            Modernizr = function () {};
        Modernizr.prototype = C, Modernizr = new Modernizr;
        var _ = n.documentElement,
            w = "svg" === _.nodeName.toLowerCase(),
            x = "Moz O ms Webkit",
            S = C._config.usePrefixes ? x.split(" ") : [];
        C._cssomPrefixes = S;
        var b = function (n) {
            var r, o = prefixes.length,
                i = e.CSSRule;
            if ("undefined" == typeof i) return t;
            if (!n) return !1;
            if (n = n.replace(/^@/, ""), r = n.replace(/-/g, "_").toUpperCase() + "_RULE", r in i) return "@" + n;
            for (var s = 0; o > s; s++) {
                var a = prefixes[s],
                    f = a.toUpperCase() + "_" + r;
                if (f in i) return "@-" + a.toLowerCase() + "-" + n
            }
            return !1
        };
        C.atRule = b;
        var E = C._config.usePrefixes ? x.toLowerCase().split(" ") : [];
        C._domPrefixes = E;
        var j = {
            elem: a("modernizr")
        };
        Modernizr._q.push(function () {
            delete j.elem
        });
        var z = {
            style: j.elem.style
        };
        Modernizr._q.unshift(function () {
            delete z.style
        }), C.testAllProps = h;
        var N = C.prefixed = function (e, n, t) {
            return 0 === e.indexOf("@") ? b(e) : (-1 != e.indexOf("-") && (e = s(e)), n ? h(e, n, t) : h(e, "pfx"))
        };
        Modernizr.addTest("objectfit", !!N("objectFit"), {
            aliases: ["object-fit"]
        }), o(), i(y), delete C.addTest, delete C.addAsyncTest;
        for (var P = 0; P < Modernizr._q.length; P++) Modernizr._q[P]();
        e.Modernizr = Modernizr
    }(window, document);

    $("html").removeClass("no-js");
    new WOW().init();

    $(".tabcordion__link").click(function (event) {
        event.preventDefault();
        tabcordion_select_link($(this).attr('href'));
    });

    var hash = window.location.hash;
    if (hash) {
        if (!$(hash).hasClass('active')) {
            tabcordion_select_link(hash);
        }

    }

    function tabcordion_select_link(href) {
        var $parent = $(href).parents('.tabcordion');
        var $newPanel = $parent.find(href);

        $(".tabcordion__link").removeClass('active');
        $parent.find('.tabcordion__link[href="' + href + '"]').addClass('active');


        if ($newPanel.hasClass('active')) {

            $(".tabcordion__link").removeClass('active');
            $newPanel.slideUp(function () {
                $newPanel.removeClass('active');
            });

        } else {
            $parent.find('.tabcordion__content.active').slideUp(function () {
                $(this).removeClass('active');
            });

            $newPanel.slideDown(function () {
                $newPanel.addClass('active');
            });
        }

        /*
        if($('.tabcordion__links').is(":visible")){

            $parent.find('.tabcordion__content').removeAttr('style');
            
            $parent.find('.tabcordion__content').removeClass('active');
            $newPanel.addClass('active');

        }else{

            if($newPanel.hasClass('active')){

                $(".tabcordion__link").removeClass('active');
                $newPanel.slideUp(function(){
                    $newPanel.removeClass('active');
                });

            }else{
                $parent.find('.tabcordion__content.active').slideUp(function(){
                    $(this).removeClass('active');
                });

                $newPanel.slideDown(function(){
                    $newPanel.addClass('active');
                });
            }
        }
        */
    }


    /*--------------------------------------------------*\
    Nice little fix for object fit in non supporting browsers
    \*--------------------------------------------------*/
    if (!$("html").hasClass('object-fit')) {
        $('.post__featured_image, .image__image').each(function () {
            var $container = $(this),
                imgUrl = $container.find('img').prop('src');
            if (imgUrl) {
                $container
                    .css('backgroundImage', 'url(' + imgUrl + ')')
                    .addClass('compat-object-fit');
            }
        });
    }


    /*--------------------------------------------------*\
    Drop Menu management code
    \*--------------------------------------------------*/
    $(".js-menu-toggle").click(function () {
        var menu = $(this).data('menu');

        if ($(menu).hasClass('active')) {
            close_menu(menu);
        } else {
            open_menu(menu);
        }
    });


    var open_menu = function (menu) {
        $(".js-menu-toggle").addClass('active');
        $(menu).addClass('active');
        $('body').addClass('no-scroll');
    };
    var close_menu = function (menu) {
        $(".js-menu-toggle").removeClass('active');
        $(menu).removeClass('active');
        $('body').removeClass('no-scroll');
    };

    /*--------------------------------------------------*\
    Smooth scrolling
    \*--------------------------------------------------*/
    $('a[href*=#]:not([href=#])').not('.tabcordion__link').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    /*--------------------------------------------------*\
    Wistia
    \*--------------------------------------------------*/
    if (typeof Wistia != 'undefined') {
        var backgroundvideoDiv = '.hero__video',
            backgroundvideo = $(backgroundvideoDiv).data('video'),
            videoOptions = {};

        Wistia.obj.merge(videoOptions, {
            plugin: {
                cropFill: {
                    src: "//fast.wistia.com/labs/crop-fill/plugin.js"
                }
            }
        });
        Wistia.embed(backgroundvideo, videoOptions);
    }

    /*--------------------------------------------------*\
    Sense Studio Showreel
    \*--------------------------------------------------*/
    $(".js-video").click(function (event) {

        event.preventDefault();
        $(this).toggleClass('hidden');

        // console.log($(this).siblings('iframe')[0]);
        var iframe = $(this).siblings('iframe')[0].contentWindow;

        // var div = document.getElementById("test");
        // var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
        iframe.postMessage('{"event":"command","func":"playVideo","args":""}', '*');

    });

    /*--------------------------------------------------*\
    Brand Panels
    \*--------------------------------------------------*/
    var $brands = $('.brands');
    var $brand_panels = $('.brands__panel');
    var $brands_nav = $('.brands__nav');

    if ($brands.length > 0) {
        var brands_top = $brands.offset().top;
        var brands_bottom = $brands.outerHeight();

        $(window).resize(function () {
            brands_top = $brands.offset().top;
            brands_bottom = $brands.outerHeight();
        });

        $(window).scroll(function () {

            var scroll_top = $(window).scrollTop();

            if (scroll_top < brands_top) {
                if ($brands_nav.hasClass('sticky')) {
                    $brands_nav.removeClass('sticky').removeClass('sticky-bottom').addClass('sticky-top');
                }
            }

            if (scroll_top > brands_top && scroll_top < brands_bottom) {
                $brands_nav.removeClass('sticky-top').addClass('sticky');
            }

            if (scroll_top > brands_bottom) {
                $brands_nav.removeClass('sticky').addClass('sticky-bottom');
            }

            var closest_distance = Math.abs(scroll_top - $($brand_panels[0]).offset().top);
            var $closest_element = $($brand_panels[0]);

            $brand_panels.each(function () {

                if (Math.abs(scroll_top - $(this).offset().top) < closest_distance) {
                    closest_distance = Math.abs(scroll_top - $(this).offset().top);
                    $closest_element = $(this);
                }

                if (closest_distance === 0) {
                    return false;
                }

            });

            var closest_index = $closest_element.data('index');

            $('.brands__nav .btn--brands').removeClass('active');
            $('.brands__nav .btn--brands:eq(' + closest_index + ')').addClass('active');

        });
    }

    /*--------------------------------------------------*\
    Staff Profiles - Shuffle
    \*--------------------------------------------------*/
    var $profiles_grid = $('.profiles');
    $profiles_grid.shuffle({
        itemSelector: '.profiles__person'
    });

    /*--------------------------------------------------*\
    Portfolio - Shuffle
    \*--------------------------------------------------*/
    var $grid = $('.portfolio_container');
    $grid.shuffle({
        itemSelector: '.portfolio_piece'
    });

    $(".cat-item").click(function (event) {
        event.preventDefault();

        $(".cat-item").removeClass('current-cat');
        $(this).addClass('current-cat');

        if ($(this).hasClass('cat-item-all')) {
            $grid.shuffle('shuffle', function () {
                return true;
            });
        } else {

            var classList = $(this).attr('class').split(/\s+/);
            $.each(classList, function (index, item) {
                if (item.substring(0, 9) === "cat-item-") {
                    $grid.shuffle('shuffle', function ($el) {
                        return $el.hasClass(item);
                    });
                }
            });
        }
    });

    /*--------------------------------------------------------*\
    Begin infinite scrolling of the blog pages
    \*--------------------------------------------------------*/
    var $blogPosts = $("#wp_posts_container");
    var $loadMoreButton = $(".js_posts_load_more");
    var $spinner = $('#inifiniteLoader');
    var currentPage = 1;
    var pages = $blogPosts.data('total');
    var ajaxurl = ajax.ajaxurl;
    var shuffleItem = 'js_shuffle_item'


    $blogPosts.imagesLoaded(function () {
        // console.log("ready");
        $blogPosts.shuffle({
            itemSelector: '.' + shuffleItem
        });
    });



    $(".blog__pagination").addClass('hidden'); //Hide the standard pagination
    $loadMoreButton.removeClass('hidden');

    $loadMoreButton.click(function () {
        currentPage++;

        if (currentPage <= pages) {
            loadArticle(currentPage);
        }

        if (currentPage >= pages) {
            $loadMoreButton.hide();
        }

    });

    function loadArticle(pageNumber) {
        $spinner.show('fast');

        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: "action=infinite_scroll&page=" + pageNumber,
            success: function (html) {

                var $items = $("");

                $.each($(html), function () {
                    if ($(this).hasClass(shuffleItem)) {

                        $items = $items.add($(this));

                    }
                });

                // $spinner.hide('1000');

                $blogPosts.append($items);

                $blogPosts.imagesLoaded(function () {
                    $blogPosts.shuffle('appended', $items);
                });

            }
        });
        return false;
    }


    var checkFormFields = function (element, event) {
        if (!!$(element).val() || event.type === 'focusin') {
            $(element).parents('.gfield').addClass('active');
        } else {
            $(element).parents('.gfield').removeClass('active');
        }
    };

    /*--------------------------------------------------------*\
    Form field statess
    \*--------------------------------------------------------*/
    $("input, textarea, select", ".ginput_container").each(function (index, el) {
        checkFormFields(el, '');
    });

    $(".ginput_container").on("focus change blur", "input, textarea, select", function (event) {
        checkFormFields($(this), event);
    });


    $(".js-hero-scroll").click(function () {
        $('html, body').animate({
            scrollTop: $(".wrap-work").offset().top
        }, 800);
    });
    
$.fn.isOnScreen = function () {

	var win = $(window);

	var viewport = {
		top: win.scrollTop(),
		left: win.scrollLeft()
	};
	viewport.right = viewport.left + win.width();
	viewport.bottom = viewport.top + win.height();
	var bounds = this.offset();
	if (bounds != undefined) {
		bounds.right = bounds.left + this.outerWidth();
		bounds.bottom = bounds.top + this.outerHeight();

		return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
	}
};


$(".cover-link").addClass("active");


$(document).on('click', '.cover-link a', function (e) {
        e.preventDefault();
    });
$(document).on('click', '.hero__arrow a', function (e) {
        e.preventDefault();
    });
	
$(document).ready(function () {
	$('.work-box').magnificPopup({
        disableOn: 767,
        removalDelay: 200,
        fixedContentPos: true,
        
        midClick: true
    });

    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    }); 
    
     
    
});

$(window).on("load", function(){
$('.grid').masonry({
  itemSelector: '.grid-item', 
});

addToAnimateClass();
    });


$(window).scroll(function () {
	addToAnimateClass();
});

function addToAnimateClass() {
	$(".grid-item").each(function () {		
		if ($(this).isOnScreen()) {
			$(this).css("opacity", 1);			
		}
	});
}
