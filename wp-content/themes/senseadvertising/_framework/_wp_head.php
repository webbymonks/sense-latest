<?php 

//XML-RPC
remove_action('wp_head', 'rsd_link');   //https://developer.wordpress.org/reference/functions/rsd_link/
//Wordpress versioning
remove_action('wp_head', 'wp_generator');   //https://developer.wordpress.org/reference/functions/wp_generator/
//Windows Live writer
remove_action('wp_head', 'wlwmanifest_link');   //https://developer.wordpress.org/reference/functions/wlwmanifest_link/

//Display relational links for the posts adjacent to the current post. - Probably best for SEO to leave this one commented
// remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);  //https://developer.wordpress.org/reference/functions/adjacent_posts_rel_link/


if(FW_RSS===false){
    //RSS
    remove_action('wp_head', 'feed_links', 2);  //https://developer.wordpress.org/reference/functions/feed_links/
    remove_action('wp_head', 'feed_links_extra', 3);    //https://developer.wordpress.org/reference/functions/feed_links_extra/
}else{
    add_theme_support( 'automatic-feed-links' );
}
if(FW_REST_API===false){
    //Outputs the REST API link tag into page header
    remove_action( 'wp_head',      'rest_output_link_wp_head'); //https://developer.wordpress.org/reference/functions/rest_output_link_wp_head/
    //Sends a Link header for the REST API
    remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 ); //https://developer.wordpress.org/reference/functions/rest_output_link_header/

    add_filter('rest_enabled', '_return_false');    //https://developer.wordpress.org/reference/hooks/rest_enabled/
    add_filter('rest_jsonp_enabled', '_return_false');  //https://developer.wordpress.org/reference/hooks/rest_jsonp_enabled/
}

if(FW_EMOJI===false){
    // all actions related to emojis
    add_action( 'init', 'fw_disable_wp_emojicons' );
    
    // filter to remove TinyMCE emojis
    add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}

function fw_disable_wp_emojicons() {
    remove_action( 'admin_print_styles',    'print_emoji_styles' );
    remove_action( 'wp_head',               'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts',   'print_emoji_detection_script' );
    remove_action( 'wp_print_styles',       'print_emoji_styles' );
    remove_filter( 'wp_mail',               'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed',      'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss',      'wp_staticize_emoji' );
}

function disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}