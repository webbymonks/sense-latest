<?php

//Begin including actions and filters to modify wordpress
add_action( 'after_setup_theme',    'fw_register_post_thumbnails' );
add_action( 'after_setup_theme',    'fw_register_nav_menus' );
add_action( 'after_setup_theme',    'fw_add_theme_support_html5' );
add_action( 'after_setup_theme',    'fw_add_theme_support_post_formats' );
add_action( 'widgets_init',         'fw_register_sidebars' );
add_action( 'template_redirect',    'fw_redirect_single_post');

add_action( 'wp_enqueue_scripts',   'fw_register_styles_and_scripts',15);
add_action( 'wp_enqueue_scripts',   'fw_enqueue_styles_and_scripts',16);

add_filter('embed_oembed_html',     'fw_wrap_iframes', 99, 4);
add_filter( 'image_send_to_editor', 'fw_wrap_images', 10, 8 );

// add_filter('excerpt_more',       'fw_change_the_excerpt'));
add_filter('excerpt_length',        'fw_change_the_excerpt_length');
add_filter('body_class',            'fw_has_sidebar');

if(FW_SEARCH===false){
    add_action( 'parse_query', 'fb_filter_query' );
    add_filter( 'get_search_form', create_function( '$a', "return null;" ) );    
}


add_theme_support( 'title-tag' );


function fw_register_styles_and_scripts(){

    /*---------------------------------------------------------------------------*\
    * Stylesheets
    ** wp_register_style( $handle, $src, $deps, $ver, $media );
    *** $media 
    **** (string) (optional) String specifying the media for which this stylesheet has been defined. Examples: 'all', 'screen', 'handheld', 'print'. See this list for the full range of valid CSS-media-types.
    **** Default: 'all'
    \*---------------------------------------------------------------------------*/

    wp_enqueue_style( 'style-landing', get_template_directory_uri() . '/landing.css'); 
    wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/src/styles/magnific-popup.css'); 
    wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/src/styles/owl.carousel.css'); 
    wp_register_style( 'style-style', get_stylesheet_uri(),array(),filemtime(get_stylesheet_directory() . '/style.css' ),'all');

  

    if(FW_FONTS!=""){
        wp_register_style( 'style-fonts', FW_FONTS, array(),'1.0','all');
    }
    
    /*---------------------------------------------------------------------------*\
    * Javascript.
    ** wp_register_script( $handle, $src, $deps, $ver, $in_footer );
    \*---------------------------------------------------------------------------*/
     wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/src/scripts/jquery.magnific-popup.min.js', array(), '1.0.0', true );
     wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/src/scripts/owl.carousel.js', array(), '1.0.0', true );
     wp_enqueue_script( 'masonry', get_template_directory_uri() . '/src/scripts/masonry.pkgd.min.js', array(), '1.0.0', true );
    wp_register_script( 'script-core', get_template_directory_uri() . '/js/script.js', array('jquery'), filemtime(get_template_directory() . '/js/script.js'), true );
    wp_register_script( 'script-map', '//maps.googleapis.com/maps/api/js', array(), '1', true );

}

function fw_enqueue_styles_and_scripts(){
    global $FW_MAPS;

    if(FW_FONTS!=""){
        wp_enqueue_style('style-fonts');
    }
    wp_enqueue_style('style-style');
    wp_enqueue_script('script-core');

    if(FW_AJAX===true){
        wp_localize_script( 'script-core', 'ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    }

    if(FW_MAPS===true || $FW_MAPS===true){
        wp_enqueue_script('script-map');
    }
}



//Register image sizes for the site
function fw_register_post_thumbnails(){
    
    global $FW_IMAGES;

    add_theme_support( 'post-thumbnails' );

    if(!empty($FW_IMAGES)){

        foreach ($FW_IMAGES as $key => $size) {

            if(empty($size[3])){
                $size[3] = false;
            }

            add_image_size(
                $size[0],
                $size[1],
                $size[2],
                $size[3]
            );
        }
    }
}




function fw_register_nav_menus(){
    global $FW_MENUS;
    if(!empty($FW_MENUS)){
        register_nav_menus($FW_MENUS);
    }
}

function fw_add_theme_support_html5(){
    if(FW_HTML5===true){
        add_theme_support( 'html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        ));
    }
}

function fw_add_theme_support_post_formats(){

    global $FW_POST_FORMATS;
    if(!empty($FW_POST_FORMATS)){
        add_theme_support('post-formats',$FW_POST_FORMATS);
    }
}

function fw_register_sidebars(){
    global $FW_SIDEBARS;
    if(!empty($FW_SIDEBARS)){
        foreach ($FW_SIDEBARS as $key => $sidebar) {
            register_sidebar($sidebar);
        }
    }

}

function fw_change_the_excerpt($more) {
    global $post;

    //if($post->post_type=="faq"){
    //  return '...<a href="javascript:void(0)">read more</a>';
    //}

    return '...';
}

function fw_change_the_excerpt_length($length) {
    global $post;
    
    // if ($post->post_type == 'post'){
    //  return 50;
    // }
    
    return $length;
}

//Redirect to the single page if search results only return one entry
function fw_redirect_single_post() {
    if (is_search()) {
        global $wp_query;
        if ($wp_query->post_count == 1 && $wp_query->max_num_pages == 1) {
            wp_redirect( get_permalink( $wp_query->posts['0']->ID ) );
            exit;
        }
    }
}

function fw_wrap_iframes($html, $url, $attr, $post_id) {

    $doc = new DOMDocument();
    $doc->loadHTML($html);

    $imageTags = $doc->getElementsByTagName('iframe')->item(0);

    $src = $imageTags->getAttribute('src');

    if (strpos($url, 'youtube') > 0) {
        $src .= "&rel=0&showinfo=0&autohide=1";
    } elseif (strpos($url, 'vimeo') > 0) {

    }

    $return = $html;

    $return = '<div class="post__iframe"><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="'.$src.'"></iframe></div></div>';

    return $return;
}

function fw_wrap_images($html, $id, $caption, $title, $align, $url, $size, $alt) {

    return '<div class="wp-image-wrap">'. $html .'</div>';

    // exit;
    // $doc = new DOMDocument();
    // $doc->loadHTML($html);

    // $imageTags = $doc->getElementsByTagName('iframe')->item(0);

    // $src = $imageTags->getAttribute('src');

    // if (strpos($url, 'youtube') > 0) {
    //     $src .= "&rel=0&showinfo=0&autohide=1";
    // } elseif (strpos($url, 'vimeo') > 0) {

    // }

    // $return = $html;

    // $return = '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="'.$src.'"></iframe></div>';

    // return $return;
}


if(!function_exists('_wp_render_title_tag')) {
    function theme_slug_render_title() { ?>
        <title>
            <?php wp_title( '|', true, 'right' ); ?>
        </title>
    <?php }
    add_action( 'wp_head', 'theme_slug_render_title' );
}

if(!isset($content_width)){
    $content_width = FW_CONTENT_WIDTH;
}


function fw_has_sidebar($classes) {
    if (is_active_sidebar(FW_PRIMARY_SIDEBAR)) {
        // add 'class-name' to the $classes array
        $classes[] = 'has_sidebar';
    }
    // return the $classes array
    return $classes;
}


function fb_filter_query( $query, $error = true ) {

    if ( is_search() ) {
        $query->is_search = false;
        $query->query_vars['s'] = false;
        $query->query['s'] = false;

        // to error
        if ( $error == true ){
            $query->is_404 = true;
        }
    }
}