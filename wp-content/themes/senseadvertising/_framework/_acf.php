<?php

$theme_settings_slug = 'theme-settings';

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_slug' 	=> $theme_settings_slug,
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
        'parent_slug'   => 'options-general.php',
	));
}