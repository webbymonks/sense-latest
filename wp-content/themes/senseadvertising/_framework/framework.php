<?php
/*------------------------------------------------------------------------*\
Define setting for the framwork
\*------------------------------------------------------------------------*/
define('FW_HTML5', true);
define('FW_EXCERPT_LENGTH', 25);
define('FW_MAPS', false);
define('FW_FONTS', "http://fast.fonts.net/cssapi/8af1ec76-e1ce-4574-ab9d-8fe4fd07c8c6.css");
define('FW_AJAX', true);
define('FW_CONTENT_WIDTH', 1170);
define('FW_RSS', false);
define('FW_REST_API', false);
define('FW_EMOJI', false);
define('FW_SEARCH', false);
define('FW_ACF_SOCIAL', true);
define("FW_PRIMARY_SIDEBAR", 'blog-sidebar-1');

//Stored as a variable as php 5.6 is required to store arrays in constants
//Shorthand array definition only support in a constant from php7 onwards
//Can use array() instead
$FW_MENUS = [
    'header'   => 'Header',
    'footer'   => 'Footer',
];

$FW_IMAGES = [
    // ['name','width','height','hard crop'],
    ['super',2000,2000,false],
    // ['super',1450,2000,false],
    ['square',600,600,true],
    ['portrait',700,840,true],
];

$FW_POST_FORMATS = [
    // 'aside',
    // 'image',
    // 'video',
    // 'quote',
    // 'link',
    // 'gallery',
    // 'status',
    // 'audio',
    // 'chat'
];

$FW_SIDEBARS = [
    array(
        'name'          => 'Blog Sidebar',
        'id'            => 'blog-sidebar-1',
        'description'   => 'Blog sidebar 1.',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    )
];

/*------------------------------------------------------------------------*\
Include the framework
\*------------------------------------------------------------------------*/
require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
require("_wp_head.php");
require("_admin.php");
require("_login.php");
require("_theme.php");
require("_acf.php");
require("_gravity_forms.php");

/*------------------------------------------------------------------------*\
Other stuff that is yet to be integrated
\*------------------------------------------------------------------------*/

function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'filter_ptags_on_images');
