<?php
add_action( 'login_enqueue_scripts', 'fw_login_stylesheet' );

function fw_login_stylesheet() {
    wp_enqueue_style( 'style-custom-login', get_template_directory_uri() . '/_framework/_login.css' );
    // wp_enqueue_script( 'custom-login', get_template_directory_uri() . '/style-login.js' );
}
