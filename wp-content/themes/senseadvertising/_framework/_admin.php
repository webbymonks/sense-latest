<?php

add_action('wp_dashboard_setup',    'fw_remove_dashboard_widgets');
add_action('admin_menu',            'fw_remove_menus' );
add_filter("wpseo_metabox_prio",    'fw_change_seo_box_priority');
add_filter('admin_footer_text',     'fw_remove_footer_admin');
//SVG FIX
add_filter('upload_mimes',          'fw_cc_mime_types');
add_action('admin_head',            'fw_fix_svg_thumb_display');
add_filter('gettext',               'fw_howdy_message', 10, 3);
// Obscure the login failure message
add_filter( 'login_errors',         'fw_login_message');

function fw_remove_dashboard_widgets(){
    global $wp_meta_boxes;

    // unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    // unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    // unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    // unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    // unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
    // unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    // unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    // unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);

}

function fw_change_seo_box_priority($pri) {
    $pri = "low";
    return $pri;
}

function fw_remove_footer_admin () {
    echo 'Theme designed and programmed by <a href="http://senseadvertising.com.au" target="_blank">Sense Advertising</a> | Made with <a href="https://wordpress.org/" target="_blank">WordPress</a>';
}

function fw_remove_menus(){

    // remove_menu_page( 'index.php' );                  //Dashboard
    // remove_menu_page( 'edit.php' );                   //Posts
    // remove_menu_page( 'upload.php' );                 //Media
    // remove_menu_page( 'edit.php?post_type=page' );    //Pages
    // remove_menu_page( 'edit-comments.php' );          //Comments
    // remove_menu_page( 'themes.php' );                 //Appearance
    // remove_menu_page( 'plugins.php' );                //Plugins
    // remove_menu_page( 'users.php' );                  //Users
    // remove_menu_page( 'tools.php' );                  //Tools
    // remove_menu_page( 'options-general.php' );        //Settings

}

function fw_cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

function fw_fix_svg_thumb_display() {
    echo '<style>
        td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { 
        width: 100% !important; 
        height: auto !important; 
        }
        </style>
    ';
}

function fw_howdy_message($translated_text, $text, $domain) {
    $new_message = str_replace('Howdy', 'Welcome', $text);
    return $new_message;
}

function fw_login_message()
{
    return '<strong>Sorry</strong>: We dont recognise that username and password combination.';
}