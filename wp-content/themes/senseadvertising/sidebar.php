<?php if ( is_active_sidebar(FW_PRIMARY_SIDEBAR) ) { ?>
    <div class="sidebar" role="complementary">
        <?php dynamic_sidebar(FW_PRIMARY_SIDEBAR); ?>
    </div>
<?php } ?>