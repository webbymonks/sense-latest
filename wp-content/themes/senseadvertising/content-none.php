<div class="container">
    <h1>We're sorry,</h1>
	<?php if(is_search()){ ?>
        <p>But nothing matched your search terms. Please try again with some different keywords.</p>
        <?php get_search_form(); ?>
    <?php }else{ ?>
        <p>It seems we can&rsquo;t find what you&rsquo;re looking for. </p>

        <?php if(FW_SEARCH){ ?>
            <p>Perhaps searching can help.</p>
            <?php get_search_form(); ?>
        <?php }else{?>
            <p>
                Please use the menu above to find the page your after.
            </p>
        <?php } ?>

    <?php } ?>
    
</div>