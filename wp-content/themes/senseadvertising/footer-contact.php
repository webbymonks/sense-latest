</div>

<footer class="footer">

    <div class="footer__bg"></div>

    <div class="footer__inner">

        <div class="footer__sub">
            <div class="container-fluid">
                <div class="row footer__contact">
                    <?php
                    $footer_panels = get_field('footer_panels');
                    if(!empty($footer_panels)) {
                        $total = count($footer_panels);
                        foreach($footer_panels as $key => $panel) { ?>
                            <div class="col-xs-12 <?php echo ($key == $total-1 ? 'col-sm-12 col-lg-5' : 'col-sm-6 col-lg-3'); ?> panel">
                                <h3 class="heading"><?php echo $panel['heading']; ?></h3>
                                <?php echo $panel['content']; ?>
                            </div>
                        <?php }
                    }
                    ?>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h4 class="social-heading">Let's be friends</h4>
                        <ul class="social-list">
                            <li>
                                <a class="btn--social socicon-facebook" href="https://www.facebook.com/senseagency" target="_blank">
                                    <span class="hover_line hover_line--small hover_line--white"></span>
                                </a>
                            </li>
                            <li>
                                <a class="btn--social socicon-twitter" href="https://twitter.com/sense_agency" target="_blank">
                                    <span class="hover_line hover_line--small hover_line--white"></span>
                                </a>
                            </li>
                            <li>
                                <a class="btn--social socicon-instagram" href="https://www.instagram.com/sense.agency" target="_blank">
                                    <span class="hover_line hover_line--small hover_line--white"></span>
                                </a>
                            </li>
                            <li>
                                <a class="btn--social socicon-linkedin" href="http://www.linkedin.com/company/433075" target="_blank">
                                    <span class="hover_line hover_line--small hover_line--white"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer__bottom">
            <div class="footer__logo">
                <a href="<?php bloginfo('url'); ?>">
                    <?php //echo file_get_contents(get_template_directory_uri().'/img/logo.svg'); ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.svg" alt="Sense Advertising" />
                </a>
            </div>
            <?php wp_nav_menu( array(
                'menu'              => 'footer',
                'theme_location'    => 'footer',
                'depth'             => 0,
                'container'         => '',
                'container_class'   => '',
                'menu_class'        => 'footer__menu',
                'fallback_cb'       => false
            )); ?>
        </div>

    </div>

</footer>

<?php wp_footer(); ?>

<div class="tracking_code_fix">
    <script type="text/javascript">
    var _ss = _ss || [];
    _ss.push(['_setDomain', 'https://koi-3QAWCD2VD4.marketingautomation.services/net']);
    _ss.push(['_setAccount', 'KOI-3QEFSEEW5K']);
    _ss.push(['_trackPageView']);
    (function() {
        var ss = document.createElement('script');
        ss.type = 'text/javascript'; ss.async = true;

        ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QAWCD2VD4.marketingautomation.services/client/ss.js?ver=1.1.1';
        var scr = document.getElementsByTagName('script')[0];
        scr.parentNode.insertBefore(ss, scr);
    })();
    </script>
    <script type="text/javascript">
        var __ss_noform = __ss_noform || [];
        __ss_noform.push(['baseURI', 'https://app-3QAWCD2VD4.marketingautomation.services/webforms/receivePostback/MzYwNzM3MjAzAwA/']);
        __ss_noform.push(['endpoint', '8a718be5-a2a4-4ebb-9cf6-880acd313297']);
    </script>
    <script type="text/javascript" src="https://koi-3QAWCD2VD4.marketingautomation.services/client/noform.js?ver=1.24" ></script>
</div>
</body>

</html>
