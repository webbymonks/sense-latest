<div class="hero <?php if(isset($video_id) && $video_id!=""){ ?>hero--video<?php } ?>">

    <style>
        <?php if($image_mobile!==""){ ?>.hero{background-image:url(<?php echo $image_mobile; ?>);}<?php } ?>
        <?php if($image_tablet!==""){ ?>@media(min-width:768px){.hero{background-image:url(<?php echo $image_tablet; ?>);}}<?php } ?>
        <?php if($image_desktop!==""){ ?>@media(min-width:1200px){.hero{background-image:url(<?php echo $image_desktop; ?>);}}<?php } ?>
    </style>

    <?php if(!empty($slideshow)) { ?>

    <div class="hero__slideshow">

        <div class="slideshow__text">
            <div class="slideshow__category text-uppercase">
                Brand
                <span class="js_typed_1">Creation</span>
            </div>
            <div class="hero__primary text-uppercase">
                Creating<br>
                <span class="js_typed_2">DESIRABILITY<br> THROUGH<br> INSIGHTS</span>
            </div>
        </div>

    </div>
    
    <?php } else { ?>

    <div class="hero__content">
        <?php if(isset($hero_title) && $hero_title!=='') { ?>
            <h1><?php echo $hero_title; ?>&nbsp;&nbsp;&mdash;</h1>
        <?php } ?>
        <?php if(isset($hero_title) && $hero_caption!=='') { ?>
            <p class="caption"><?php echo $hero_caption; ?></p>
        <?php } ?>
    </div>

    <?php } ?>

    <?php if(isset($video_id) && $video_id!="") { ?>
        <div class="hero__video" id="wistia_<?php echo esc_attr($video_id); ?>" data-video="<?php echo esc_attr($video_id); ?>"></div>
        <script charset='ISO-8859-1' src='//fast.wistia.com/assets/external/E-v1.js'></script>
        <script charset='ISO-8859-1' src='//fast.wistia.com/labs/crop-fill/plugin.js'></script>
    <?php } ?>

    <div class="hero__arrow arrow_animated floating infinite">
        <button class="js-hero-scroll">
            <?php echo file_get_contents(get_template_directory_uri().'/img/portfolio_arrow.svg'); ?>
        </button>
    </div>

</div>