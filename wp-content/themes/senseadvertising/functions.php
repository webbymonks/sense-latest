<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

require("_framework/framework.php");

add_filter( 'body_class', 'my_class_names' );
function my_class_names( $classes ) {

    if(defined('HERO') && HERO===true){
        $classes[] = 'has_hero';
    }

    if(is_single() && has_term( 'case-studies','speciality') ) {
        $classes[] = 'has_case_study';
    }

    return $classes;
}
function featured_work() {

?>


<?php 
 }

add_shortcode('featured_work', 'featured_work' );
/*--------------------------------------------------------*\
Modifies the query on the product archive and product taxonomy page to show all products
\*--------------------------------------------------------*/
add_action( 'pre_get_posts', 'my_modify_main_query');
function my_modify_main_query( $query ) {

    /*--------------------------------------------------------*\
    Determine if we are on the archive page for products or on the taxonomy page for type
    \*--------------------------------------------------------*/
    if(
        isset($query->query['post_type']) && $query->query['post_type']=='portfolio' ||
        isset($query->query['speciality'])){
            $query->query_vars['posts_per_page'] = -1;
    }

}


function get_featured_url($post, $image_size){

    if (has_post_thumbnail($post->ID)){
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $image_size );
        return $image[0];
    }

    return false;
}


// require get_template_directory(). "/vendor/autoload.php";
// use MetzWeb\Instagram\Instagram;

// function get_instagram_feed($account_name, $delete=false){
//     $instagram_transient    = 'instagram_data_'.$account_name;
//     $instagram_data         = null;
//     $api_key                = "92d27012882d475fbea213ee1cd444aa";
//     $api_secret             = "af7b84abac984b7db7348984668e11c6";
//     $api_redirect_url       = "http://sense.com.au";

//     if($delete===true){
//         delete_transient($instagram_transient);
//     }

//     if(get_transient( $instagram_transient )!==false){
//         return unserialize(get_transient($instagram_transient));
//     }else{
//         $instagram          = new Instagram($api_key, $api_secret, $api_redirect_url);
//         $account            = $instagram->searchUser($account_name,1);
//         $account_id         = $account->data[0]->id;

//         $instagram_data     = $instagram->getUserMedia($account_id,9);
//         set_transient( $instagram_transient, serialize($instagram_data), HOUR_IN_SECONDS);
//         return $instagram_data;
//     }
// }


function wp_infinitepaginate(){
    $page           = $_POST['page'];
    $posts_per_page  = get_option('posts_per_page');

    # Load the posts
    query_posts(array('paged' => $page ));
    get_template_part( 'loop' );

    exit;
}

add_action('wp_ajax_infinite_scroll', 'wp_infinitepaginate');           // for logged in user
add_action('wp_ajax_nopriv_infinite_scroll', 'wp_infinitepaginate');    // if user not logged in

function my_mce_before_init_insert_formats( $init ) {

    $style_formats = array(
        array(
            'title'  => 'Extra wide image', // Title to show in dropdown
            'selector'  => 'div', // Element to add class to
            'classes' => 'post__extra_wide_image' // CSS class to add
        ),
    );

    $init['style_formats'] = json_encode( $style_formats );

    return $init;
}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

function remove_hashtags($string){
    return preg_replace('/#(?=[\w-]+)/', '',
        preg_replace('/(?:#[\w-]+\s*)+$/', '', $string));
}
add_filter( 'gform_ajax_spinner_url', 'spinner_url', 10, 2 );
function spinner_url( $image_src, $form ) {
    return "https://senseadvertising.com.au/wp-content/plugins/gravityforms/images/spinner.gif";
}
/***************** AJAX Load more **************************/

add_action ( 'wp_ajax_nopriv_my_load_ajax', 'my_load_ajax' );
add_action ( 'wp_ajax_my_load_ajax', 'my_load_ajax' );


function my_load_ajax()
{
	 $post_id = $_POST['postid'];
?><button title="Close (Esc)" type="button" class="mfp-close">×</button>
<ul class="popup-slider cf">
                         
                        <li class="pop<?php echo $post_id;?>">
                            <?php if(!has_term( 'case-studies','speciality') ) { ?>
                                       <div class="container-fluid">
            <?php } ?>

            <article id="post-<?php $post_id; ?>" <?php post_class(); ?>>

                <?php if(has_term( 'case-studies','speciality') ) { ?>
                <div class="hero portfolio_hero" style="background-image:url(<?php echo esc_attr(get_featured_url($post_id,'super')); ?>);">
                    <div class="portfolio_hero__content remove_children_first_last_margin text-center text-uppercase">
                        <h1 class="portfolio_hero__heading">
                            <span class="h3">Case Study</span>
                            <span class="h2"><?php the_title($post_id); ?></span>
                            <button class="js-hero-scroll arrow arrow_animated floating infinite">
                                <?php echo file_get_contents(get_template_directory().'/img/portfolio_arrow.svg'); ?>
                            </button>
                        </h1>
                    </div>
                </div>
                <?php } ?>

                <?php if(has_term( 'case-studies','speciality') ) { ?>
                    <div class="container-fluid">
                <?php } else { ?>
                    <div class="portfolio__featured object_fit_image object_fit_image--56 object_fit_image--mobile-full-width">
                        <?php echo get_the_post_thumbnail($post_id,'super'); ?>
                    </div>
                <?php } ?>

                <div class="row portfolio__top_content" id="portfolio_content">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7 col-xl-6">

                        <?php if(!has_term( 'case-studies','speciality') ) { ?>
                            <h1 class="h2 bold portfolio__heading no_margin text-uppercase">
                                <?php echo get_the_title($post_id); ?>
                            </h1>
                            <h2 class="portfolio__subheading"><?php echo get_the_excerpt($post_id); ?></h2>
                            <?php
                                          $pop_cont = $post_id;// example post id
                                          $post_content = get_post($pop_cont);
                                          $content = $post_content->post_content;
                                         
                                          ?>
                        <p><?php echo $content; ?></p>
                        <?php } else { ?>
                            <div class="portfolio__intro"><?php echo get_the_content($post_id); ?></div>
                        <?php } ?>

                        <?php if(has_term( 'case-studies','speciality') ) { ?>
                        <div class="tabcordion">

                            <ul class="tabcordion__links">
                                <li>
                                    <a class="tabcordion__link active" href="#brief">The Brief</a>
                                </li>
                                <li>
                                    <a class="tabcordion__link" href="#solution">The Solution</a>
                                </li>
                                <li>
                                    <a class="tabcordion__link" href="#outcome">The Outcome</a>
                                </li>
                            </ul>

                            <div class="tabcordion__block">

                                <a href="#brief" class="tabcordion__link active">
                                    The Brief
                                </a>

                                <div id="brief" class="tabcordion__content active">
                                    <?php the_field('brief',$post_id); ?>
                                </div>

                            </div>

                            <div class="tabcordion__block">

                                <a href="#solution" class="tabcordion__link">
                                    The Solution
                                </a>

                                <div id="solution" class="tabcordion__content">
                                    <?php the_field('solution',$post_id); ?>
                                </div>

                            </div>

                            <div class="tabcordion__block">

                                <a href="#outcome" class="tabcordion__link">
                                    The Outcome
                                </a>

                                <div id="outcome" class="tabcordion__content">
                                    <?php the_field('outcome',$post_id); ?>
                                </div>

                            </div>
                        </div>
                        <?php } ?>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 col-lg-offset-2 col-xl-3 col-xl-offset-3">
                        <div class="what_we_did what_we_did--1 tt_uppercase fw_light ls_s small color--pink">
                            <div class="what_we_did__heading">What we did:</div>
                            <?php
                            $items = explode("\n",get_field('what_we_did',$post_id));
                            if(!empty($items)) { ?>
                                <ul>
                                <?php foreach($items as $item) { ?>
                                    <li><?php echo $item; ?></li>
                                <?php } ?>
                                </ul>
                            <?php } ?>
                        </div>
                        <?php if(get_field('url',$post_id) != '') { ?>
                        <div class="portfolio__url">
                            <a href="<?php echo get_field('url',$post_id); ?>" target="_blank">
                            <?php
                            if(get_field('url_visual',$post_id) != '') {
                                echo get_field('url_visual',$post_id);
                            } else {
                                echo get_field('url',$post_id);
                            }
                            ?>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>

                <?php
                $layouts = get_field('images',$post_id);
                if(!empty($layouts)){?>
                    <div class="images">

                        <?php foreach ($layouts as $key => $layout) {
                            $switch_sides = false;
                            if(isset($layout['portrait_side']) && $layout['portrait_side']==='right'){
                                $switch_sides = true;
                            }

                            $offset = 0;

                            ?><div class="row images__group images__group--<?php echo esc_attr($layout['acf_fc_layout']); ?>"><?php

                                if($layout['acf_fc_layout']==='landscape'){
                                    $image = $layout['image'];
                                    ?>
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="object_fit_image image__image image__image--landscape" >

                                            <img class="img-fluid" src="<?php echo esc_attr($image['sizes']['super']); ?>" alt="<?php echo esc_attr($image['alt']); ?>">
                                        </div>
                                    </div>
                                    <?php

                                }elseif($layout['acf_fc_layout']==='native_size'){
                                    $image = $layout['image'];
                                    ?>
                                    <div class="col-xs-12 col-sm-12">
                                        <div >
                                            <img class="img-fluid" src="<?php echo esc_attr($image['sizes']['super']); ?>" alt="<?php echo esc_attr($image['alt']); ?>">
                                        </div>
                                    </div>
                                    <?php

                                }elseif($layout['acf_fc_layout']==='portraits'){
                                    $left_image = $layout['left_image'];
                                    $right_image = $layout['right_image'];
                                    ?>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="object_fit_image image__image image__image--portrait">
                                            <img class="img-fluid" src="<?php echo esc_attr($left_image['sizes']['super']); ?>" alt="<?php echo esc_attr($left_image['alt']); ?>">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6">
                                        <div class="object_fit_image image__image image__image--portrait " >
                                            <img class="img-fluid" src="<?php echo esc_attr($right_image['sizes']['super']); ?>" alt="<?php echo esc_attr($right_image['alt']); ?>">
                                        </div>
                                    </div>
                                    <?php
                                }elseif($layout['acf_fc_layout']==='landscapes'){
                                    $left_image = $layout['left_image'];
                                    $right_image = $layout['right_image'];
                                    ?>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="object_fit_image image__image image__image--landscape " >
                                            <img class="img-fluid" src="<?php echo esc_attr($left_image['sizes']['super']); ?>" alt="<?php echo esc_attr($left_image['alt']); ?>">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6">
                                        <div class="object_fit_image image__image image__image--landscape " >
                                            <img class="img-fluid" src="<?php echo esc_attr($right_image['sizes']['super']); ?>" alt="<?php echo esc_attr($right_image['alt']); ?>">
                                        </div>
                                    </div>
                                    <?php
                                }elseif($layout['acf_fc_layout']==='portrait_and_landscapes'){

                                    $portrait_image = $layout['portrait_image'];
                                    $top_landscape_image = $layout['top_landscape_image'];
                                    $bottom_landscape_image = $layout['bottom_landscape_image'];
                                    ?>
                                    <div class="col-xs-12 col-sm-6<?php if($switch_sides===true){?> col-sm-push-6<?php } ?>">
                                        <div class="object_fit_image image__image image__image--portrait " >
                                            <img class="img-fluid" src="<?php echo esc_attr($portrait_image['sizes']['super']); ?>" alt="<?php echo esc_attr($portrait_image['alt']); ?>">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6<?php if($switch_sides===true){?> col-sm-pull-6<?php } ?>">
                                        <div class="object_fit_image image__image image__image--landscape " >
                                            <img class="img-fluid" src="<?php echo esc_attr($top_landscape_image['sizes']['super']); ?>" alt="<?php echo esc_attr($top_landscape_image['alt']); ?>">
                                        </div>
                                        <div class="object_fit_image image__image image__image--landscape image__spacer " >
                                            <img class="img-fluid" src="<?php echo esc_attr($bottom_landscape_image['sizes']['super']); ?>" alt="<?php echo esc_attr($bottom_landscape_image['alt']); ?>">
                                        </div>
                                    </div>
                                    <?php
                                }elseif($layout['acf_fc_layout']==='portrait_and_landscape'){

                                    $portrait_image = $layout['portrait_image'];
                                    $landscape_image = $layout['landscape_image'];
                                    ?>

                                    <div class="col-xs-12 col-sm-4<?php if($switch_sides===true){?> col-sm-push-8<?php } ?>">
                                        <div class="object_fit_image image__image image__image--portrait ">
                                            <img class="img-fluid" src="<?php echo esc_attr($portrait_image['sizes']['super']); ?>" alt="<?php echo esc_attr($portrait_image['alt']); ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8<?php if($switch_sides===true){?> col-sm-pull-4<?php } ?>">
                                        <div class="object_fit_image image__image image__image--landscape " >
                                            <img class="img-fluid" src="<?php echo esc_attr($landscape_image['sizes']['super']); ?>" alt="<?php echo esc_attr($landscape_image['alt']); ?>">
                                        </div>
                                    </div>
                                    <?php
                                } elseif($layout['acf_fc_layout']==='portrait_portrait_portrait'){

                                    $left_image = $layout['left_image'];
                                    $middle_image = $layout['middle_image'];
                                    $right_image = $layout['right_image'];
                                    ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="object_fit_image image__image image__image--portrait " >
                                            <img class="img-fluid" src="<?php echo esc_attr($left_image['sizes']['super']); ?>" alt="<?php echo esc_attr($left_image['alt']); ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="object_fit_image image__image image__image--portrait " >
                                            <img class="img-fluid" src="<?php echo esc_attr($middle_image['sizes']['super']); ?>" alt="<?php echo esc_attr($middle_image['alt']); ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="object_fit_image image__image image__image--portrait " >
                                            <img class="img-fluid" src="<?php echo esc_attr($right_image['sizes']['super']); ?>" alt="<?php echo esc_attr($right_image['alt']); ?>">
                                        </div>
                                    </div>
                                    <?php
                                }elseif($layout['acf_fc_layout']==='four_portraits'){

                                    $image_1 = $layout['image_1'];
                                    $image_2 = $layout['image_2'];
                                    $image_3 = $layout['image_3'];
                                    $image_4 = $layout['image_4'];
                                    ?>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="object_fit_image image__image image__image--portrait" >
                                            <img class="img-fluid" src="<?php echo esc_attr($image_1['sizes']['super']); ?>" alt="<?php echo esc_attr($image_1['alt']); ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="object_fit_image image__image image__image--portrait ">
                                            <img class="img-fluid" src="<?php echo esc_attr($image_2['sizes']['super']); ?>" alt="<?php echo esc_attr($image_2['alt']); ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="object_fit_image image__image image__image--portrait ">
                                            <img class="img-fluid" src="<?php echo esc_attr($image_3['sizes']['super']); ?>" alt="<?php echo esc_attr($image_3['alt']); ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="object_fit_image image__image image__image--portrait ">
                                            <img class="img-fluid" src="<?php echo esc_attr($image_4['sizes']['super']); ?>" alt="<?php echo esc_attr($image_4['alt']); ?>">
                                        </div>
                                    </div>
                                    <?php
                                } elseif($layout['acf_fc_layout']==='video'){
                                    $video_id = $layout['video_id'];
                                    $autoplay = $layout['autoplay'];
                                    $image = $layout['image'];

                                    if(isset($layout['color']) && $layout['color']!=""){
                                        $color = $layout['color'];
                                    }else{
                                        $color="#000";
                                    }

                                    if(isset($layout['text']) && $layout['text']!=""){
                                        $text = $layout['text'];
                                    }else{
                                        $text = 'Play Video';
                                    }
                                    ?>
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="object_fit_image image__image image__video image__image--landscape ">

                                            <iframe class="video-mobile hidden-md-up" id="video_<?php echo esc_attr($key); ?>" src="https://player.vimeo.com/video/<?php echo $video_id; ?>?api=1" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                            <iframe class="video-desktop hidden-sm-down" id="video_<?php echo esc_attr($key); ?>" src="https://player.vimeo.com/video/<?php echo $video_id; ?>?api=1<?php echo ($autoplay == 'Y' ? '&autoplay=1' : '&autoplay=0&loop=0'); ?>" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                                            <?php if($autoplay == 'N') { ?>
                                            <div class="video__overlay hidden-sm-down" style="background-image:url('<?php echo $image['sizes']['large']; ?>');">
                                                <button class="video__link js-play-portfolio-video">
                                                    <span class="text-uppercase h2 video__button bold" style="color:<?php echo $color; ?>"><?php echo $text; ?></span>
                                                </button>
                                            </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                    <?php
                                }
                            ?></div><?php
                        }
                    ?>
                    </div>
                <?php } ?>

                <?php if(has_term( 'case-studies','speciality') ) { ?>
                    </div>
                <?php } ?>


            </article>
            <?php if(!has_term( 'case-studies','speciality') ) { ?>
                </div>
                            <?php } ?>
            </li>
           <?php wp_reset_postdata();?>
            
        </ul>
<?php die();
}