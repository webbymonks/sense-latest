<?php
/**
 * Template Name:Landing
 */
define('HERO', true);

$bg_img = get_field('hero_image');
$insight_bg_img = get_field('insights_background');

get_header(); ?>

<section class="wrap-landing" style="background-image: url(<?php echo $bg_img['url']?>)">
    <div class="cover-hero-cont">
        <?php echo get_field('banner_text'); ?>
    </div>
    <div class="hero__arrow arrow_animated floating infinite">
        <a class="" href="#first-sec">
            <?php echo file_get_contents(get_template_directory_uri().'/img/portfolio_arrow.svg'); ?>
        </a>
    </div>
    <div class="cover-link">
        <a href="#enquire-now" title="Enquire Now">Enquire Now</a>
    </div>
</section>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<section class="wrap-work" id="first-sec">
       <div class="container">
        <div class="row">
            <div class="col-12 to-load" data-load="3">
                <div class="cover-title">
                    <h2>
                        <?php echo get_field('section_title')?>
                    </h2>
                </div>
                <div class="cover-work grid clearfix laodcontent">
                    
                     <?php
                       $featuredproject = get_field('portfolio_selection');
                        
                        foreach($featuredproject as $page_field){
                            $post_id=$page_field;
                            $image= get_field('portfolio_hero_image',$post_id);
                                
                            ?>

<div class="grid-item to-load--item">
                        <a href="#pop<?php echo $post_id?>" id="" title="" class="work-box" onClick="get_pipup_ajax_data(<?php echo $post_id?>);">
                            <img src="<?php echo $image['url']; ?>" alt="">
                        </a>
                         <div id="pop<?php echo $post_id?>" class="mfp-hide white-popup-block">
            <!--        <a class="popup-modal-dismiss" href="javascript:;">x</a>-->
            <div class="loder_image">
                        <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
  <path fill="#000" d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
    <animateTransform attributeType="xml"
      attributeName="transform"
      type="rotate"
      from="0 25 25"
      to="360 25 25"
      dur="0.6s"
      repeatCount="indefinite"/>
    </path>
  </svg>
                </div>
                    
                
                    
        
                       </div>
               

                    </div>
                       <?php  }
                        
                        
                           
                            ?>
                    <!--<div class="grid-item">
                        <a href="#sliderPopup" id="pop<?php// echo $post_id?>" title="" class="work-box">
                            <img src="<?php //echo $url; ?>" alt="">
                        </a>
                    </div>-->

                    

                </div>
                
            
            </div>
        </div>
    </div>
   
</section>
<section class="wrap-client">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 cover-logos-box">
                <div class="cover-title">
                    <h2>
                        <?php echo get_field('title_section'); ?>
                    </h2>
                </div>
                <?php if(!empty(get_field('client_logo'))) { ?>
                <ul class="cover-logos">
                    <?php foreach(get_field('client_logo') as $logo_img){
                    $client_logo = $logo_img['logo'];
                    ?>
                    <li><a href="<?php echo $logo_img['website_link']; ?>" target="_blank"><img src="<?php echo $client_logo['url']; ?>" alt="<?php echo $client_logo['title']; ?>" /></a></li>
                    <?php } 
                    ?>
                </ul>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<section class="wrap-services">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 cover-title">
                <h2>
                    <?php echo get_field('service_section_title'); ?>
                </h2>
            </div>
            <?php foreach(get_field('service_box_repeater') as $service_cover){ 
            $service_img = $service_cover['stage_image'];
            $service_title = $service_cover['stage_title'];
            ?>
            <div class="col-lg-4 col-xs-12 cover-sevices">
                <figure>
                    <span class="cover-img"><img src="<?php echo $service_img['url']; ?>" alt="<?php echo $service_img['title']; ?>" /></span>
                    <figcaption>
                        <h3>
                            <?php echo $service_title; ?>
                        </h3>
                        <?php if(!empty($service_cover['stage_list_repeater'])) { ?>
                        <ul>
                            <?php foreach($service_cover['stage_list_repeater'] as $service_list){ 
                            $service_cont = $service_list['list_field'];
                            
                            ?>
                            <li>
                                <?php echo $service_cont; ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </figcaption>
                </figure>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<section class="wrap-insights" style="background-image: url(<?php echo $insight_bg_img['url']?>)">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 cover-insights">
                <div class="cover-title">
                    <h2>
                        <?php echo get_field('trending_insights_title'); ?>
                    </h2>
                </div>
                <div class="insight-cont">
                    <?php echo get_field('trending_insights_content'); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="wrap-footer">
    <div class="footer__inner">
        <div class="footer__sub" id="enquire-now">
            <div class="container-fluid">
                <div class="row">
                    <?php $con_info = get_field('contact_info');	?>
                    <div class="col-lg-6 col-xs-12 footer__contact">
                        <div class="foot-title">
                            <?php echo $con_info['contact_title_and_content']; ?>
                        </div>
                        <?php if(!empty($con_info['director'])) { ?>
                        <ul class="cover-directors">
                            <?php foreach($con_info['director'] as $director_cover){ 
                            $director_img = $director_cover['director_image'];
                            $director_name = $director_cover['name'];
                            $director_position = $director_cover['position'];
                            $director_number = $director_cover['contact_number'];
                            $director_email = $director_cover['email'];
							$num_str = str_replace(" ","",$director_number);

                            ?>
                            <li class="direct-data">
                                <figure>
                                    <img src="<?php echo $director_img['url']; ?>" alt="<?php echo $director_img['title']; ?>" />
                                    <figcaption>
                                        <span class="dir-name">
                                            <?php echo $director_name; ?></span>
                                        <span class="dir-pos">
                                            <?php echo $director_position; ?></span>
                                        <a href="tel:<?php echo $num_str; ?>" class="dir-num">
                                            <?php echo $director_number; ?></a>
                                        <a href="mailto:<?php echo $director_email; ?>" class="dir-email">
                                            <?php echo $director_email; ?></a>
                                    </figcaption>
                                </figure>
                            </li>

                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </div>
                    <?php $form_info = get_field('contact_form');	?>
                    <div class="col-xs-12 col-lg-6 footer-form">
                        <div class="foot-title">
                            <?php echo $form_info['contact_form_title']; ?>
                        </div>
                        <div class="foot-form-cover">
                            <?php echo do_shortcode($form_info['contact_form_short_code']);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer__bottom">
            <div class="footer__logo">
                <a href="<?php bloginfo('url'); ?>">
                    <?php //echo file_get_contents(get_template_directory().'/img/logo.svg'); ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.svg" alt="Sense Advertising" />
                </a>
            </div>
            <?php wp_nav_menu( array(
                    'menu'              => 'footer',
                    'theme_location'    => 'footer',
                    'depth'             => 0,
                    'container'         => '',
                    'container_class'   => '',
                    'menu_class'        => 'footer__menu',
                    'fallback_cb'       => false
                )); ?>
        </div>

    </div>
</footer>
<script>
function get_pipup_ajax_data(potid)
{
    //alert(potid);
     var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        var data = {
            'action' : 'my_load_ajax', // function.php function name
            'postid' : potid     // pass value with ajax action
        };
        jQuery.post(ajaxurl, data, function(response) { 
             //alert(response);  
            $('#pop'+potid).html(response);  // add response on given id    
        });
}
</script>
<?php wp_footer(); ?>
