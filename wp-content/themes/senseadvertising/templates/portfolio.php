<?php
/**
 * Template Name: Portfolio
 */

get_header(); ?>

<div class="container-fluid portfolio">
    <h1 class="h2 section--heading color--pink">Work</h1>

    <ul class="text-xs-center list-inline fw_light hidden-sm-down">
        <li class="cat-item cat-item-all<?php if(is_post_type_archive('portfolio')){?> current-cat<?php } ?>">
            <a href="<?php echo get_post_type_archive_link('portfolio'); ?>">All</a>
        </li>
        <?php
        //exclude "case-studies" need to change this
        wp_list_categories(array(
            'orderby'            => 'term_order',
            'title_li'           => '',
            'exclude'            => array(11),
            'taxonomy'           => 'speciality'
        ));
        ?>
        <li class="cat-item cat-item-video">
            <a href="<?php echo get_post_type_archive_link('portfolio'); ?>">TV / Video</a>
        </li>
    </ul>

    <div class="portfolio_container clearfix">

    <?php if(have_posts()) { $key = 0; while ( have_posts() ) { the_post(); ?>

        <?php
        $terms = get_the_terms($post, 'speciality');
        $post_terms = [];
        if(!empty($terms)) {
            foreach ($terms as $key => $term) {
                array_push($post_terms, "cat-item-".$term->term_id);
            }
        }

        $case_study = false;
        if(has_term('case-studies','speciality')){
            $case_study = true;
        }



        $orientation = get_field('orientation');
        if($orientation == 'portrait') {
            //use featured portrait image
            $image = get_field('featured_portrait_image',$post->ID);

            if(!empty($image)) {
                $image = $image['sizes']['portrait'];
            }else{
                $image = get_featured_url($post,'medium');
            }

        } else {
            //use featured landscape image
            $image = get_field('featured_landscape_image',$post->ID);
            if(!empty($image)) {
                // $image = get_field('featured_landscape_image',$post->ID);
                $image = $image['sizes']['medium'];
            } else {
                //use post featured image
                $image = get_featured_url($post,'medium');
            }
        }

        ?>

        <article class="portfolio_piece <?php if($case_study===true){?>portfolio_piece--case-study <?php } ?><?php echo esc_attr(implode(' ',$post_terms)); ?>">
            <a class="portfolio_piece__link portfolio_piece--<?php echo esc_attr($orientation); ?>" href="<?php the_permalink(); ?>">
                <div class="portfolio_piece__image" style="background-image:url(<?php echo esc_attr($image); ?>);"></div>
                <div class="portfolio_piece__content">
                    <h1 class="h3 line-before portfolio_piece__title"><?php the_title(); ?></h1>
                    <div class="hidden-lg-down">
                        <?php the_excerpt(); ?>
                    </div>
                </div>
            </a>
        </article>

        <?php $key++; } } ?>

    </div>

    <?php
    $portfolio_videos = get_field('portfolio_videos','options');
    if(!empty($portfolio_videos)) {
    ?>
    <div class="video_container hidden">
        <div class="row">
            <?php foreach($portfolio_videos as $index => $video) { ?>
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <div class="video_container__video wow fadeInUp">
                        <div class="object_fit_image image__image image__video image__image--landscape">
                            <iframe class="video-desktop" id="video_<?php echo esc_attr($index); ?>" src="https://player.vimeo.com/video/<?php echo $video['vimeo_id']; ?>?api=1&autoplay=0&loop=0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                        <h3 class="h3 bold video-title"><?php echo $video['title']; ?></h3>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>

    <!-- <button class="hidden js_posts_load_more font--soho color--blue featured--link--alt">Load more</button> -->

</div>

<?php get_footer();
