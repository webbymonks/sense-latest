<?php
/**
 * Template Name: Services
 */

define('HERO', true);

get_header(); ?>

<?php
$slideshow = get_field('slideshow');

$image_mobile = get_field('mobile_hero_image');
if(!empty($image_mobile)) {
    $image_mobile = $image_mobile['sizes']['medium'];
} else {
    $image_mobile = '';
}

$image_tablet = get_field('tablet_hero_image');
if(!empty($image_mobile)) {
    $image_tablet = $image_tablet['sizes']['large'];
} else {
    $image_tablet = '';
}

$image_desktop = get_featured_url($post,'super');

$hero_title = get_field('hero_title');
$hero_caption = get_field('hero_caption');

$video_id       = "6l7az76tvt";
$hero_title     = get_field('hero_title');
//$hero_caption   = get_field('hero_caption');
$hero_caption = 'Strong brands are built on <a href="#strategy">strategic</a> direction, <a href="#creative">creative</a><br /> flair and <a href="#digital">digital</a> expertise. Our services are versatile,<br /> because strong brands touch every facet of life.';

$image_slideshow = false;

include('partials/_hero.php');
?>

<?php
$service_areas = get_field('service_areas');
if(!empty($service_areas)) { ?>
    <div class="container-fluid">
    <?php foreach ($service_areas as $key => $area) { ?>

        <?php
        global $post;
        $post = $area['featured_case_study'];
        setup_postdata( $post );

        $image = get_field('featured_square_image');
        $imageSrc = $image['sizes']['square'];

        $color = 'color--blue';
        if($key%2==0) {
            $color = 'color--pink';
        }
        ?>

        <div id="<?php echo esc_attr(strtolower($area['title'])); ?>" class="service<?php echo ($key % 2 == 0 ? ' service--alt' : ''); ?>">

            <div class="service__top">
                <div class="service__description h2">
                    <h2 class="h1 text-uppercase">
                        <?php echo $area['title']; ?>
                    </h2>
                    <div class="<?php echo $color; ?>">
                        <?php echo $area['description']; ?>
                    </div>
                </div>

                <div class="service__specialties tt_uppercase fw_light ls_s lh_l small <?php echo $color; ?>">
                    <?php echo $area['specialities']; ?>
                </div>
            </div>

            <div class="service__case_study wow fadeInUp" data-wow-delay="0.25s">

                <div class="service__case_study_image">
                    <img class="img-fluid" src="<?php echo $imageSrc; ?>" alt="<?php echo $image['alt']; ?>">
                </div>

                <div class="service__case_study_content">
                    <h3 class="h2 bold text-uppercase service__case_study_heading">
                        <span class="<?php echo $color; ?>"><?php echo $area['title']; ?> case study</span><br>
                        <?php the_title(); ?>
                    </h3>
                    <?php the_content(); ?>
                    <a class="<?php echo $color; ?> service__case_study_link font--soho text-uppercase" href="<?php the_permalink(); ?>">
                        Read more
                        <div class="hover_line"></div>
                    </a>
                </div>

            </div>
        </div>

        <?php wp_reset_postdata(); ?>
    <?php } ?>
    </div>
    <?php
}
?>

<?php
//--------------------------------------------------------
// More Case Studies
//--------------------------------------------------------
$case_studies = get_field('case_studies');
if(!empty($case_studies)) { ?>
<div class="container-fluid recent_work recent_work--case-studies text-center wow fadeInUp">
    <h2 class="recent_work__heading text-uppercase bold color--blue">More Case Studies</h2>
    <div class="row">
    <?php foreach ($case_studies as $index => $post) {
        setup_postdata( $post ); ?>
        <div class="col-xs-12 col-sm-6 recent_work__inner">
            <?php include('partials/_work_piece.php'); ?>
        </div>
        <?php
        wp_reset_postdata();
    }
    ?>
    </div>
    <a class="recent_work__button font--soho color--blue featured--link--alt" href="<?php echo get_post_type_archive_link('portfolio'); ?>">View All Work</a>
</div>
<?php } ?>

<?php get_footer();
