<?php
/**
* Template Name: Digital Climate Survey
*/

get_header('clean'); ?>

<?php while ( have_posts() ) { the_post(); ?>

<div class="digital_climate_survey">

    <header class="digital_climate_survey__header">
        <div class="container">
            <div class="digital_climate_survey__header__inner">
                <div class="digital_climate_survey__header__logo">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg">
                </div>
            </div>
        </div>
    </header>

    <div class="container">

        <div class="row digital_climate_survey__boxes no_last_margin">


            <!-- ------------------------------------------------------------ -->
            <!-- Start of magic box -->
            <!-- ------------------------------------------------------------ -->
            <div class="col-xs-12 col-lg-4 col-lg-push-4 js_digital_climate_survey_magic_box">

                <div class="digital_climate_survey__box bottom_space digital_climate_survey__box--up digital_climate_survey__box--yellow digital_climate_survey__box--phone hidden-md-down">
                    <div class="digital_climate_survey__box__hover digital_climate_survey__box__hover--blue">
                        <p class="digital_climate_survey__h2">
                            Sense is
                        </p>
                        <p>
                            a multi-disciplinary creative agency based in Fitzroy, Melbourne. We’ve helped build brands for 10 years across the digital and traditional environments with award-winning integrated campaigns for major clients across Australia.
                        </p>
                    </div>
                </div>

                <p class="digital_climate_survey__h1 digital_climate_survey__special_heading bottom_space bold">
                    Do digital right in 2017
                </p>

                <div class="digital_climate_survey__wave">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/graphic_blue_wave.svg">
                </div>

                <div class="digital_climate_survey__box digital_climate_survey__box--light_blue digital_climate_survey__box--special digital_climate_survey__h2 bold no_last_margin">
                    <p>
                        Complete our 5 minute survey for your chance to win and receive a free copy of the report.
                    </p>
                </div>

                <div class="digital_climate_survey__light_blue_box__after">
                    <div class="digital_climate_survey__wave">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/graphic_blue_wave.svg">
                    </div>
                </div>

            </div>

            <!-- ------------------------------------------------------------ -->
            <!-- End of magic box -->
            <!-- ------------------------------------------------------------ -->

            <div class="col-xs-12 col-lg-4 col-lg-pull-4 magic_top_margin">
                <div class="digital_climate_survey__box digital_climate_survey__box--blue digital_climate_survey__h2">
                    <img class="digital_climate_survey__box__graphic" src="<?php echo get_template_directory_uri(); ?>/img/graphic_book.svg">
                    <p>
                        Our report covers trends and opportunities in digital media what you’ll need to do digital right in 2017
                    </p>
                </div>
            </div>

            <div class="col-xs-12 col-lg-4 magic_top_margin">
                <div class="digital_climate_survey__box digital_climate_survey__box--lines digital_climate_survey__box--pink digital_climate_survey__h2 bold">
                    <img class="digital_climate_survey__box__graphic" src="<?php echo get_template_directory_uri(); ?>/img/graphic_win.svg">
                    <p>
                        We’ll also enter you into the draw to win 20 hours of strategic digital planning with Damien Cheney, Digital Director at Sense
                    </p>
                    <img class="digital_climate_survey__box__graphic" src="<?php echo get_template_directory_uri(); ?>/img/graphic_person.svg">
                </div>
            </div>

        </div>
    </div>

    <footer class="digital_climate_survey__footer">
        <div class="digital_climate_survey__footer__button no_last_margin">
            <p>
                <a href="https://www.surveymonkey.net/r/?sm=ejRTIGpffO24h7f11ajmdI_2Bo5YIp5O83YA3SDHDjfg8_3D" class="digital_climate_survey__button">
                    <span class="hover_line"></span>
                    Begin the survey
                </a>
            </p>
        </div>
        <div class="container no_last_margin digital_climate_survey__footer__black">
            <p>
                If you want to know more, say <a href="mailto:hello@sense.com.au?subject=Digital Climate Survey">hello@sense.com.au</a>,<br>
                or visit <a href="http://senseadvertising.com.au">senseadvertising.com.au</a>
            </p>
        </div>
    </footer>
</div>

<?php } ?>

<?php get_footer('clean');
