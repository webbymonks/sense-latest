<?php
/**
 * Template Name: Contact
 */

get_header(); ?>

<?php while ( have_posts() ) { the_post(); ?>

<div class="container-fluid">

    <h1 class="h2 section--heading color--pink"><?php the_title(); ?></h1>

    <div class="row intro hidden-lg-up">
        <div class="col-xs-12">
            <?php the_content(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6">
            <div class="intro hidden-md-down">
                <?php the_content(); ?>
            </div>
            <?php
            if(get_field('show_form')===true){
                $form = get_field('form');
                if(!empty($form)){
                    gravity_form_enqueue_scripts($form->id, true);
                    gravity_form($form->id, false, true, false, '', false, 0);

                    ?>
                    <script type="text/javascript">
                    jQuery(document).bind('gform_confirmation_loaded', function(event, formId){
                        if(formId==<?php echo $form->id; ?>){
                            ga('send', 'pageview', '/contact/thank-you-message');
                        }
                    });
                    </script>
                    <?php
                }
            }
            ?>

        </div>
        <div class="col-xs-12 col-md-6 col-lg-6 col-xl-5 col-xl-offset-1 location-map hide_br_small_screens">
            <p>We are located just off Brunswick Street. Level 2, 106 Victoria Street, Fitzroy 3065</p>
            <a href="https://www.google.com.au/maps/place/Sense+Advertising,+2%2F106+Victoria+St,+Fitzroy+VIC+3065/" target="_blank">
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/map.png" alt="">
            </a>
            <a class="map__link font--soho text-uppercase color--blue" href="https://www.google.com.au/maps/place/Sense+Advertising,+2%2F106+Victoria+St,+Fitzroy+VIC+3065/" target="_blank">
               <div class="hover_line"></div>
               View Google Map
           </a>
        </div>
    </div>


</div>

<?php } ?>

<?php get_footer('contact');
