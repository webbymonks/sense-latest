<?php
/**
 * Template Name: Instagram
 */
error_reporting(E_ALL); ini_set('display_errors', 1);
define('HERO', true);

require get_template_directory() . '/instagram/Instagram.php';

use MetzWeb\Instagram\Instagram;

get_header(); ?>

<?php

$image_mobile   = get_field('mobile_hero_image');
if(!empty($image_mobile) && isset($image_mobile['sizes'])) {
    $image_mobile   = $image_mobile['sizes']['medium'];
} else {
    $image_mobile = '';
}

$image_tablet   = get_field('tablet_hero_image');
if(!empty($image_tablet) && isset($image_tablet['sizes'])) {
    $image_tablet   = $image_tablet['sizes']['large'];
} else {
    $image_tablet = '';
}

$image_desktop  = get_featured_url($post,'super');
$slideshow      = true;
$video_id       = "364ptktwz5";
$hero_title     = get_field('hero_title');
$hero_caption   = get_field('hero_caption');

$image_slideshow = true;

include('partials/_hero.php');
?>

<?php while(have_posts()) { the_post(); ?>

<div class="container-fluid section services text-center">

    <div class="services__inner">

        <div class="services__content">
            <h2 class="alpha services__heading">Our Services</h2>
            <a class="font--soho color--blue featured--link--alt" href="<?php echo get_permalink(get_page_by_title('services')); ?>">Learn More</a>
        </div>

        <div class="services__svg_container">
            <div class="services__svg_inner">
                <svg class="services__svg" id="services-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 417.5 515.9">
                    <path id="ampersand__path_1" class="scrollimate st0" d="M155.1,221.5l-87.6,55c-58.3,36.7-75.9,113.6-39.3,172c36.7,58.3,113.6,75.9,172,39.3l142.3-89.5c20.2-12.7,46.8-6.6,59.5,13.6c12.7,20.2,6.6,46.8-13.6,59.5c-20.2,12.7-46.8,6.6-59.5-13.6L317,439" />
                    <path id="ampersand__path_2" class="scrollimate st0" d="M190.8,199l53.9-33.9c39.5-24.8,51.4-77,26.7-116.5c-24.9-39.5-77-51.5-116.6-26.7c-39.5,24.9-51.5,77-26.7,116.6l166.4,264.8" />
                </svg>
            </div>
        </div>

        <?php
        $services = get_field('services');
        if(!empty($services)) {
            foreach ($services as $index => $service) { ?>
                <a href="<?php echo esc_url(get_permalink(get_page_by_title('services'))."#".strtolower($service['name'])); ?>" class="services__service services__service--<?php echo $index; ?> wow hidden">
                    <h3 class="h2 services__title color--pink text-uppercase bold"><?php echo $service['name']; ?></h3>

                    <div class="services__service_content">
                        <div class="hover_line hover_line--long-small"></div>

                        <p>
                        <?php echo $service['description']; ?>
                        </p>
                    </div>

                </a>
            <?php }
        }
        ?>
    </div>
</div>



<div class="our_work">

    <h2 class="container-fluid our_work__heading text-center alpha">Our Work</h2>

    <?php
    //--------------------------------------------------------
    // Featured Projects
    //--------------------------------------------------------
    $featured_project = get_field('featured_project');
    if(!empty($featured_project)) { ?>

        <h3 class="h2 container-fluid featured_projects__heading color--pink text-uppercase bold text-center">Featured Projects</h3>

        <div class="featured_projects container-fluid wow fadeInUp text-center">

            <button class="featured_projects__button featured_projects__button--prev">
                <span class="sr-only">Previous Project</span>
            </button>
            <button class="featured_projects__button featured_projects__button--next">
                <span class="sr-only">Next Project</span>
            </button>

            <div class="featured_projects__inner">

                <div class="featured_projects__image_container">
                    <div class="js_featured_projects_image_container">
                        <?php foreach ($featured_project as $index => $post) { setup_postdata( $post ); ?>
                            <?php $image = get_field('featured_project_image'); ?>
                            <img class="img-fluid" src="<?php echo $image['sizes']['medium']; ?>">
                        <?php wp_reset_postdata(); } ?>
                    </div>
                </div>

                <div class="featured_projects__content_container">

                    <div class="js_featured_projects_content_container">
                        <?php foreach ($featured_project as $index => $post) { setup_postdata( $post ); ?>
                            <div class="featured_projects__content_inner">
                                <h3 class="h2 featured_projects__project_title text-uppercase bold"><?php the_title(); ?></h3>
                                <div class="featured_projects__project_content">
                                    <p><?php echo get_field('featured_project_text'); ?></p>
                                </div>
                                <a class="featured_projects__project_link font--soho color--blue text-uppercase" href="<?php the_permalink(); ?>">
                                    View Project
                                    <div class="hover_line hover_line--white"></div>
                                </a>
                            </div>
                        <?php wp_reset_postdata(); } ?>
                    </div>
                </div>
            </div>

       </div>

    <?php } ?>

    <?php
    //--------------------------------------------------------
    // Sense Studio Showreel
    //--------------------------------------------------------
    ?>

    <div class="video wow fadeInUp">

        <h2 class="video__heading text-uppercase h2 color--pink">Our<br /> Showreel</h2>

        <div class="video__inner">
            <span class="corner"></span>
            <div class="video__player">

                <iframe class="video__video" src="https://player.vimeo.com/video/171026141?api=1" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                <video id="showreel" class="showreel hidden-sm-down" preload="auto">
                    <source src="<?php echo get_template_directory_uri(); ?>/media/showreel.mp4" type="video/mp4">
                    <div class="video__placeholder"></div>
                </video>

                <div class="video__placeholder"></div>

                <button class="video__link js-play-video hidden-sm-down">
                    <span class="text-uppercase h2 video__button bold">Play Video</span>
                </button>

                <script>
                    var video = document.getElementById('showreel');
                    var played = false;
                    function checkScroll() {

                        var video_top = jQuery('.video__player').offset().top;

                        if(jQuery(window).scrollTop() > video_top - (jQuery(window).height() / 2)) {
                            if(played == false) {
                                video.play();
                                played = true;
                            }
                        }

                    }

                    window.addEventListener('scroll', checkScroll, false);
                    //window.addEventListener('resize', checkScroll, false);
                </script>

            </div>
        </div>

    </div>

    <?php
    //--------------------------------------------------------
    // Recent Work
    //--------------------------------------------------------
    $recent_work = get_field('recent_work');
    if(!empty($recent_work)) { ?>
    <div class="container-fluid recent_work text-center wow fadeInUp">
        <h2 class="recent_work__heading text-uppercase bold color--pink">Recent Work</h2>
        <div class="row">
        <?php foreach ($recent_work as $index => $post) {
            setup_postdata( $post ); ?>
            <div class="col-xs-12 col-sm-6 recent_work__inner">
                <?php include('partials/_work_piece.php'); ?>
            </div>
            <?php
            wp_reset_postdata();
        }
        ?>
        </div>
        <a class="recent_work__button font--soho color--blue featured--link--alt" href="<?php echo get_post_type_archive_link('portfolio'); ?>">More Work</a>
    </div>
    <?php } ?>
</div>

<?php
//--------------------------------------------------------
// Instagram
//--------------------------------------------------------
// $feed = get_instagram_feed("sense.agency",true);

/*

require_once get_template_directory().'/instagram/lib/Service.php';
require_once get_template_directory().'/instagram/lib/services/instagram.php';

$instagram = new InstagramService();
$feed = $instagram->get_feed();

var_dump($feed);

if($feed!==false){ ?>
    <div class="container-fluid section instagram text-center">

        <div class="instagram__inner wow fadeInUp">
            <h2 class="alpha instagram__heading">
                Instagram
            </h2>

            <ol class="instagram__list">
                <?php foreach($feed->data as $key => $picture) {
                    $image      = $picture->images->standard_resolution->url;
                    $link       = $picture->link;
                    $timestamp  = $picture->created_time;
                    $caption    = remove_hashtags($picture->caption->text);
                    $likes      = $picture->likes->count;

                    ?>
                    <li>
                        <a href="<?php echo esc_attr($link); ?>" target="_blank" class="instagram__item" style="background-image:url(<?php echo esc_attr($image); ?>);">
                            <div class="instagram__content">
                                <time datetime="<?php echo date_i18n( 'Y-m-d H:s', $timestamp); ?>">
                                    <?php echo date_i18n( get_option( 'date_format' ), $timestamp); ?>
                                </time>

                                <?php echo $caption; ?>

                                <div class="instagram__heart">
                                    <div class="instagram__heart_inner">
                                        <?php echo $likes; ?>
                                    </div>
                                </div>

                            </div>
                        </a>
                    </li>
                    <?php
                    //limit to 3
                    if($key >= 2) {
                        break;
                    }
                }
                ?>
            </ol>

           <a class="instagram__link font--soho text-uppercase" href="https://www.instagram.com/sense.agency" target="_blank">
               <div class="hover_line"></div>
               View our feed
           </a>
       </div>
    </div>
<?php } */ ?>
<?php 
// Initialize class
$instagram = new Instagram(array(
  'apiKey'      => '1f8f43bf5b444ae8bfccf7721dd80c2a',
  'apiSecret'   => '53394416166345d593a3219f703aeea4',
  'apiCallback' => 'http://senseadvertising.com.au/'
));
// Receive OAuth code parameter
$code = $_GET['code'];
// Check whether the user has granted access
if (true === isset($code)) {
  // Receive OAuth token object
  $data = $instagram->getOAuthToken($code);
  // Store user access token
  $instagram->setAccessToken($data);
  // Now you can call all authenticated user methods
  // Get the most recent media published by a user
  $media = $instagram->getUserMedia('self', 3);
  foreach ($media->data as $entry) {
    echo "<img src=\"{$entry->images->thumbnail->url}\">";
  }
} ?>

<?php } ?>

<?php get_footer();
