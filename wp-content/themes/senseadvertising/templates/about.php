<?php
/**
 * Template Name: About
 */

define('HERO', true);

get_header(); ?>

<?php
$slideshow = get_field('slideshow');

$image_mobile = get_field('mobile_hero_image');
if(!empty($image_mobile)) {
    $image_mobile = $image_mobile['sizes']['medium'];
} else {
    $image_mobile = '';
}

$image_tablet = get_field('tablet_hero_image');
if(!empty($image_mobile)) {
    $image_tablet = $image_tablet['sizes']['large'];
} else {
	$image_tablet = '';
}

$image_desktop = get_featured_url($post,'super');

$hero_title = get_field('hero_title');
$hero_caption = get_field('hero_caption');

$video_id       = "qmgi8y9b1l";
$hero_title     = get_field('hero_title');
//$hero_caption   = get_field('hero_caption');
$hero_caption = 'We <a href="#brand-0" data-smooth-center="true">create</a>, <a href="#brand-1" data-smooth-center="true">roll out</a> and <a href="#brand-2" data-smooth-center="true">sell</a> strong brands<br /> to a waiting world. A strong brand<br /> benefits its owners - our clients.';

$image_slideshow = false;

include('partials/_hero.php');
?>

<?php while(have_posts()) { the_post(); ?>
    <div class="brands">
	    <div class="container-fluid">

			<?php

			$brands = get_field('brand_panels');

			foreach($brands as $index => $panel) { ?>

			<div class="brands__panel brands--<?php echo $panel['colour']; ?>" id="brand-<?php echo $index; ?>" data-index="<?php echo $index; ?>">
				<div class="row">
					<div class="col-xs-12 col-lg-3 col-lg-offset-1">
						<h2 class="brands__heading">
							<span class="brand-top color--<?php echo $panel['colour']; ?> wow slideInLeft">Brand</span>
							<span class="brand-bottom wow slideInLeft"><?php echo $panel['title']; ?></span>
							<div class="line wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.5s"></div>
						</h2>
					</div>
					<div class="col-xs-12 col-lg-6 col-lg-offset-1">
						<p class="intro font-large color--<?php echo $panel['colour']; ?>"><?php echo $panel['intro']; ?></p>
						<p>&mdash; <?php echo $panel['content']; ?></p>
					</div>
				</div>
			</div>

			<?php } ?>

			<div class="brands__animation sticky-top">
				<div class="brands__animation--creation">
					<div class="brand-svg brand-creation active">
						<img src="<?php echo get_template_directory_uri(); ?>/img/animations/brand-creation.svg" alt="" />
					</div>
				</div>
				<div class="brands__animation--rollout">
					<div class="brand-svg brand-rollout-a">
						<img src="<?php echo get_template_directory_uri(); ?>/img/animations/brand-rollout-a.svg" alt="" />
					</div>
					<div class="brand-svg brand-rollout-b">
						<img src="<?php echo get_template_directory_uri(); ?>/img/animations/brand-rollout-b.svg" alt="" />
					</div>
				</div>
				<div class="brands__animation--sell">
					<div class="brand-svg brand-sell-a">
						<img src="<?php echo get_template_directory_uri(); ?>/img/animations/brand-sell-a.svg" alt="" />
					</div>
					<div class="brand-svg brand-sell-b">
						<img src="<?php echo get_template_directory_uri(); ?>/img/animations/brand-sell-b.svg" alt="" />
					</div>
				</div>
			</div>

		</div>

	</div>

</div>

<div class="main main--people scroll-fix">
	<div class="container-fluid">
        <h2 class="section--heading color--blue"><?php echo get_field('people_title'); ?></h2>
        <div class="row">
			<div class="col-xs-12 col-sm-6 people_content">
				<?php echo get_field('people_content'); ?>
			</div>
			<div class="col-xs-12 col-sm-6"></div>
        </div>
		<div class="profiles">
        <?php
        $people = get_field('people');

        $chunkedPeople = array(
            '2_columns' => array(
                '1' => array(),
                '2' => array(),
            ),
            '3_columns' => array(
                '1' => array(),
                '2' => array(),
                '3' => array(),
            ),
            '4_columns' => array(
                '1' => array(),
                '2' => array(),
                '3' => array(),
                '4' => array(),
            ),
        );

        foreach ($people as $key => $person) {

            if($key%2==0){
                array_push($chunkedPeople['2_columns']['1'], $person);
            }else{
                array_push($chunkedPeople['2_columns']['2'], $person);
            }

            if($key%3==0){
                array_push($chunkedPeople['3_columns']['1'], $person);
            }elseif($key%3==1){
                array_push($chunkedPeople['3_columns']['2'], $person);
            }elseif($key%3==2){
                array_push($chunkedPeople['3_columns']['3'], $person);
            }

            if($key%4==0){
                array_push($chunkedPeople['4_columns']['1'], $person);
            }elseif($key%4==1){
                array_push($chunkedPeople['4_columns']['2'], $person);
            }elseif($key%4==2){
                array_push($chunkedPeople['4_columns']['3'], $person);
            }elseif($key%4==3){
                array_push($chunkedPeople['4_columns']['4'], $person);
            }
        }

        global $post;

        ?>
        <div class="row profiles__row hidden-md-up">
            <?php foreach ($chunkedPeople['2_columns'] as $key => $column) {
                ?>
                <div class="col-xs-6 profiles__column people_column--2-<?php echo $key; ?>">
                <?php
                foreach ($column as $key => $post) {
                    setup_postdata( $post );
                    ?>
                        <div class="profiles__person wow fadeInUp js_person">
                            <div class="profiles__image object_fit_image object_fit_image--125">
                                <?php the_post_thumbnail('rectangle-sm'); ?>
                            </div>
                            <h3 class="title"><?php the_title(); ?></h3>
                            <p class="position"><?php echo get_field('position'); ?></p>
                        </div>
                    <?php
                    wp_reset_postdata();
                }
                ?>
                </div>
                <?php
            } ?>
        </div>

        <div class="row profiles__row hidden-sm-down visible-md-up hidden-xl-up">
            <?php foreach ($chunkedPeople['3_columns'] as $key => $column) {
                ?>
                <div class="col-xs-4 profiles__column people_column--3-<?php echo $key; ?>">
                <?php
                foreach ($column as $key => $post) {
                    setup_postdata( $post );
                    ?>
                        <div class="profiles__person wow fadeInUp">
                            <div class="profiles__image object_fit_image object_fit_image--125">
                                <?php the_post_thumbnail('rectangle-sm'); ?>
                            </div>
                            <h3 class="title"><?php the_title(); ?></h3>
                            <p class="position"><?php echo get_field('position'); ?></p>
                            <?php the_content(); ?>
                        </div>
                    <?php
                    wp_reset_postdata();
                }
                ?>
                </div>
                <?php
            } ?>
        </div>

        <div class="row profiles__row hidden-lg-down visible-xl-up">
            <?php foreach ($chunkedPeople['4_columns'] as $key => $column) {
                ?>
                <div class="col-xs-3 profiles__column people_column--4-<?php echo $key; ?>">
                <?php
                foreach ($column as $key => $post) {
                    setup_postdata( $post );
                    ?>
                        <div class="profiles__person wow fadeInUp">
                            <div class="profiles__image object_fit_image object_fit_image--125">
                                <?php the_post_thumbnail('rectangle-sm'); ?>
                            </div>
                            <h3 class="title"><?php the_title(); ?></h3>
                            <p class="position"><?php echo get_field('position'); ?></p>
                            <?php the_content(); ?>
                        </div>
                    <?php
                    wp_reset_postdata();
                }
                ?>
                </div>
                <?php
            } ?>
        </div>

        <?php /* foreach($people as $index => $post) {
        	setup_postdata( $post ); ?>
        	<div class="profiles__person">
        		<div class="profiles__image object_fit_image object_fit_image--125">
                    <?php the_post_thumbnail('rectangle-sm'); ?>
    			</div>
    			<h3 class="title"><?php the_title(); ?></h3>
    			<p class="position"><?php echo get_field('position'); ?></p>
    			<div class="hidden-xs-down">
    				<?php the_content(); ?>
        		</div>
        	</div>
        	<?php wp_reset_postdata();
        } */?>
        </div>
	</div>

	<script>
	jQuery(document).ready(function($) {

		var $window = $(window),
            $document = $(document);

        /*--------------------------------------------------*\
        Brand Animations
        \*--------------------------------------------------*/
        var brand_rollout_trigger_1 = $('#brand-0').offset().top + ($('#brand-0').outerHeight() / 2) - 100;
        var brand_rollout_trigger_2 = $('#brand-0').offset().top + ($('#brand-0').outerHeight() / 2);
        var brand_sell_trigger_1 = $('#brand-1').offset().top + ($('#brand-1').outerHeight() / 2) - 100;
        var brand_sell_trigger_2 = $('#brand-1').offset().top + ($('#brand-1').outerHeight() / 2);

        $window.scroll(function() {

        	if($window.scrollTop() <= brand_rollout_trigger_1) {
        		$('.brand-svg').removeClass('active');
        		$('.brand-creation').addClass('active');
        	}

        	if($window.scrollTop() >= brand_rollout_trigger_1 && $window.scrollTop() < brand_rollout_trigger_2) {
        		$('.brand-svg').removeClass('active');
        		$('.brand-rollout-a').addClass('active');
        	}

        	if($window.scrollTop() >= brand_rollout_trigger_2 && $window.scrollTop() < brand_sell_trigger_1) {
        		$('.brand-svg').removeClass('active');
        		$('.brand-rollout-b').addClass('active');
        	}

        	if($window.scrollTop() >= brand_sell_trigger_1 && $window.scrollTop() < brand_sell_trigger_2) {
        		$('.brand-svg').removeClass('active');
        		$('.brand-sell-a').addClass('active');
        	}

        	if($window.scrollTop() >= brand_sell_trigger_2) {
        		$('.brand-svg').removeClass('active');
        		$('.brand-sell-b').addClass('active');
        	}

        });

	});
	</script>

<?php } ?>

<?php get_footer();
