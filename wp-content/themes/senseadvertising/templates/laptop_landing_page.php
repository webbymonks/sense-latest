<?php
/**
 * Template Name: Laptop Landing Page
 */

get_header(); ?>

<?php while ( have_posts() ) { the_post(); ?>

<div class="container laptop_landing_page">

    <a href="<?php bloginfo('url'); ?>" class="header__logo laptop_landing_page__logo">
        <?php echo file_get_contents(get_template_directory()."/img/logo.svg"); ?>
    </a>

    <h1 class="laptop_landing_page__heading color--pink text-uppercase extra-bold super lh_s">
        <?php the_title(); ?>
    </h1>

    <div class="laptop_landing_page__laptop">
        <div class="laptop_landing_page__laptop_inner">
            <div class="row laptop_landing_page__row">
                <div class="col-xs-12 col-lg-7">
                    <div class="laptop_landing_page__inner laptop_landing_page__inner--left no_last_margin">
                        <?php the_content(); ?>

                        <?php if(get_field('button_link')!="" && get_field('button_text')!=""){ ?>
                            <p class="laptop_landing_page__button_wrapper">
                                <a target="_blank" class="laptop_landing_page__button" href="<?php the_field('button_link'); ?>">
                                    <span class="hover_line"></span>
                                    <?php the_field('button_text'); ?>
                                </a>
                            </p>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-5">
                    <div class="laptop_landing_page__inner laptop_landing_page__inner--right color--blue no_last_margin">
                        <?php if(get_field('right_heading')!=""){ ?>
                            <div class="laptop_landing_page__right_heading bold">
                                <?php the_field('right_heading'); ?>
                            </div>
                        <?php } ?>

                        <?php the_field('right_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php } ?>

<?php get_footer();
