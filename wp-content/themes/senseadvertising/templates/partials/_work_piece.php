<a class="recent_work__item" href="<?php the_permalink(); ?>">
    <div class="recent_work__image <?php if($index==0){ ?>recent_work__image--triangle<?php } ?> object_fit_image object_fit_image--56 object_fit_image--mobile-full-width">
        <?php the_post_thumbnail('medium'); ?>
        <div class="recent_work__overlay">
            <div class="recent_work__content">
                <?php the_excerpt(); ?>
            </div>
        </div>
    </div>

    <h3 class="recent_work__title">
        <div class="hover_line"></div>
        <?php if(get_field('category')){
            the_field('category');
        }else{
            the_title();
        } ?>
    </h3>
    <div class="recent_work__excerpt">
        <?php the_excerpt(); ?>
    </div>

</a>
