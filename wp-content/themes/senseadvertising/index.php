<?php get_header(); ?>
<div class="container-fluid">

    <h1 class="h2 section--heading blog--heading color--pink">Blog</h1>
    
    <?php if(have_posts()) { ?>
        
        <div id="wp_posts_container" data-total="<?php echo $wp_query->max_num_pages; ?>" class="row blog__container">
        
            <?php get_template_part('loop'); ?>

        </div>

        <div class="blog__loadmore">
            <button class="js_posts_load_more font--soho color--blue featured--link--alt">Load more</button>
            <div class="loader">
                <div id="inifiniteLoader" class="sk-cube-grid">
                    <div class="sk-cube sk-cube1"></div>
                    <div class="sk-cube sk-cube2"></div>
                    <div class="sk-cube sk-cube3"></div>
                    <div class="sk-cube sk-cube4"></div>
                    <div class="sk-cube sk-cube5"></div>
                    <div class="sk-cube sk-cube6"></div>
                    <div class="sk-cube sk-cube7"></div>
                    <div class="sk-cube sk-cube8"></div>
                    <div class="sk-cube sk-cube9"></div>
                </div>
            </div> 
        </div>

        <style>
            #inifiniteLoader {
            z-index: 2;  
            display: none;
        }
        </style>

    <?php } else {
        get_template_part( 'content', 'none' );
    } ?>
</div>
<?php get_footer();