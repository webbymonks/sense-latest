<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php

    if(!isset($showSingle)) {
        $showSingle = true;
    }

    if(is_singular('post') && $showSingle===true) { ?>

        <div class="post__featured_image object_fit_image object_fit_image--50 object_fit_image--mobile-full-width">
            <?php the_post_thumbnail('super'); ?>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-push-4 col-lg-6 col-lg-push-5 wp_content">
                <h1 class="post__title">
                    <?php the_title(); ?>
                </h1>

                <div class="hidden-lg-up post__excerpt h3">
                    <?php the_excerpt(); ?>
                </div>

                <time class="post__time hidden-sm-up color--pink line-before font--soho text-uppercase " datetime="<?php the_time('Y-m-d'); ?>"><?php the_time( get_option( 'date_format' ) ); ?></time>

                <?php the_content(); ?>

                <ul class="share_links">
                    <li>
                        <a class="socicon-facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_attr(get_the_permalink()); ?>">
                            <span class="hover_line hover_line--small"></span>
                            <!-- <span class="sr-only">Facebook</span> -->
                        </a>
                    </li>
                    <li>
                        <a class="socicon-twitter" target="_blank" href="https://twitter.com/home?status=<?php echo esc_attr(get_the_permalink()); ?>">
                            <span class="hover_line hover_line--small"></span>
                            <!-- <span class="sr-only">Twitter</span> -->
                        </a>
                    </li>
                </ul>

                <a class="view_all_link font--soho color--blue text-uppercase" href="<?php if(get_option( 'show_on_front' ) == 'page' ){ echo get_permalink( get_option('page_for_posts' ) ); }else{ echo bloginfo('url'); } ?>">
                    Back to Posts
                    <div class="hover_line hover_line--white"></div>
                </a>

            </div>
            <div class="col-xs-12 col-md-4 col-md-pull-8 col-lg-4 col-lg-pull-6 hidden-sm-down">

                <time class="post__time color--pink line-before font--soho text-uppercase ls_m" datetime="<?php the_time('Y-m-d'); ?>"><?php the_time( get_option( 'date_format' ) ); ?></time>

                <div class="hidden-md-down post__excerpt h3">
                    <?php the_excerpt(); ?>
                </div>
                <?php
                global $post;
                $post = get_field('author');
                setup_postdata( $post );

                ?>
                <div class="author">
                    <div class="author__image">
                        <?php
                        $image = get_field('square_image');
                        if(!empty($image)){
                            echo wp_get_attachment_image( $image, 'thumbnail', false, array(
                                'class'=>'img-fluid'
                            ));
                        }else{
                            the_post_thumbnail('thumbnail',array(
                                'class'=>'img-fluid'
                            ));
                        }
                        ?>
                    </div>
                    <div class="author__content small">
                        <strong><?php the_title(); ?></strong><br>
                        <?php the_field('position') ?>
                    </div>
                </div>
                <?php
                wp_reset_postdata();
                ?>
            </div>
        </div>

    <?php } else { ?>
        <a class="post_tile" href="<?php the_permalink(); ?>">
            <?php
            $image_type = get_field('image_type');
            ?>
            <div class="post_tile__image post_tile__image--<?php echo esc_attr($image_type); ?>">
                <?php
                if($image_type === 'square' || $image_type === 'circle') {
                    $image_type = 'square';
                } else {
                    $image_type = 'medium';
                }
                the_post_thumbnail($image_type);
                ?>
            </div>

            <h1 class="post_tile__title h2">
                <?php the_title(); ?>
            </h1>

            <?php the_excerpt(); ?>

            <div class="post_tile__readmore font--soho text-uppercase color--blue small">
                Read more
                <div class="hover_line"></div>
            </div>
        </a>
    <?php } ?>
</article>
