<?php get_header(); ?>

<?php if(have_posts()) {
    while(have_posts()) { the_post();
        get_template_part('content','portfolio');


        ?>
        <div class="container-fluid fw_light">
            <?php the_post_navigation( array(
                'next_text' => 'Next<span class="hover_line hover_line--small"></span>',
                'prev_text' => '<span class="hover_line hover_line--small"></span>Prev',
            ) ); ?>
        </div>
        <?php
    }
}?>

<?php get_footer();