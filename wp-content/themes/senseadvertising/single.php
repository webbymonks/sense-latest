<?php get_header(); ?>

<div class="container-fluid">
    <?php while(have_posts()) { the_post();
        get_template_part('content');
    } ?>

    <div class="fw_light">
        <?php the_post_navigation( array(
            'next_text' => 'Next<span class="hover_line hover_line--small"></span>',
            'prev_text' => '<span class="hover_line hover_line--small"></span>Prev',
        ) ); ?>
    </div>

</div>

<?php get_footer();