<!DOCTYPE html>
<!--[if IE 9]>
<html <?php language_attributes(); ?> class="ie9 no-js">
<![endif]-->
<!--[if !IE]> -->
<html <?php language_attributes(); ?> class="no-js">
<!-- <![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <script>
    /*! modernizr 3.3.1 (Custom Build) | MIT *
     * https://modernizr.com/download/?-objectfit-touchevents-setclasses !*/
    !function(e,n,t){function r(e,n){return typeof e===n}function o(){var e,n,t,o,i,s,a;for(var f in g)if(g.hasOwnProperty(f)){if(e=[],n=g[f],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(o=r(n.fn,"function")?n.fn():n.fn,i=0;i<e.length;i++)s=e[i],a=s.split("."),1===a.length?Modernizr[a[0]]=o:(!Modernizr[a[0]]||Modernizr[a[0]]instanceof Boolean||(Modernizr[a[0]]=new Boolean(Modernizr[a[0]])),Modernizr[a[0]][a[1]]=o),y.push((o?"":"no-")+a.join("-"))}}function i(e){var n=_.className,t=Modernizr._config.classPrefix||"";if(w&&(n=n.baseVal),Modernizr._config.enableJSClass){var r=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");n=n.replace(r,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(n+=" "+t+e.join(" "+t),w?_.className.baseVal=n:_.className=n)}function s(e){return e.replace(/([a-z])-([a-z])/g,function(e,n,t){return n+t.toUpperCase()}).replace(/^-/,"")}function a(e,n){return!!~(""+e).indexOf(n)}function f(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):w?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function l(e,n){return function(){return e.apply(n,arguments)}}function u(e,n,t){var o;for(var i in e)if(e[i]in n)return t===!1?e[i]:(o=n[e[i]],r(o,"function")?l(o,t||n):o);return!1}function c(e){return e.replace(/([A-Z])/g,function(e,n){return"-"+n.toLowerCase()}).replace(/^ms-/,"-ms-")}function p(){var e=n.body;return e||(e=f(w?"svg":"body"),e.fake=!0),e}function d(e,t,r,o){var i,s,a,l,u="modernizr",c=f("div"),d=p();if(parseInt(r,10))for(;r--;)a=f("div"),a.id=o?o[r]:u+(r+1),c.appendChild(a);return i=f("style"),i.type="text/css",i.id="s"+u,(d.fake?d:c).appendChild(i),d.appendChild(c),i.styleSheet?i.styleSheet.cssText=e:i.appendChild(n.createTextNode(e)),c.id=u,d.fake&&(d.style.background="",d.style.overflow="hidden",l=_.style.overflow,_.style.overflow="hidden",_.appendChild(d)),s=t(c,e),d.fake?(d.parentNode.removeChild(d),_.style.overflow=l,_.offsetHeight):c.parentNode.removeChild(c),!!s}function m(n,r){var o=n.length;if("CSS"in e&&"supports"in e.CSS){for(;o--;)if(e.CSS.supports(c(n[o]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var i=[];o--;)i.push("("+c(n[o])+":"+r+")");return i=i.join(" or "),d("@supports ("+i+") { #modernizr { position: absolute; } }",function(e){return"absolute"==getComputedStyle(e,null).position})}return t}function v(e,n,o,i){function l(){c&&(delete T.style,delete T.modElem)}if(i=r(i,"undefined")?!1:i,!r(o,"undefined")){var u=m(e,o);if(!r(u,"undefined"))return u}for(var c,p,d,v,h,y=["modernizr","tspan","samp"];!T.style&&y.length;)c=!0,T.modElem=f(y.shift()),T.style=T.modElem.style;for(d=e.length,p=0;d>p;p++)if(v=e[p],h=T.style[v],a(v,"-")&&(v=s(v)),T.style[v]!==t){if(i||r(o,"undefined"))return l(),"pfx"==n?v:!0;try{T.style[v]=o}catch(g){}if(T.style[v]!=h)return l(),"pfx"==n?v:!0}return l(),!1}function h(e,n,t,o,i){var s=e.charAt(0).toUpperCase()+e.slice(1),a=(e+" "+S.join(s+" ")+s).split(" ");return r(n,"string")||r(n,"undefined")?v(a,n,o,i):(a=(e+" "+j.join(s+" ")+s).split(" "),u(a,n,t))}var y=[],g=[],C={_version:"3.3.1",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){g.push({name:e,fn:n,options:t})},addAsyncTest:function(e){g.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=C,Modernizr=new Modernizr;var _=n.documentElement,w="svg"===_.nodeName.toLowerCase(),x="Moz O ms Webkit",S=C._config.usePrefixes?x.split(" "):[];C._cssomPrefixes=S;var b=function(n){var r,o=P.length,i=e.CSSRule;if("undefined"==typeof i)return t;if(!n)return!1;if(n=n.replace(/^@/,""),r=n.replace(/-/g,"_").toUpperCase()+"_RULE",r in i)return"@"+n;for(var s=0;o>s;s++){var a=P[s],f=a.toUpperCase()+"_"+r;if(f in i)return"@-"+a.toLowerCase()+"-"+n}return!1};C.atRule=b;var j=C._config.usePrefixes?x.toLowerCase().split(" "):[];C._domPrefixes=j;var z={elem:f("modernizr")};Modernizr._q.push(function(){delete z.elem});var T={style:z.elem.style};Modernizr._q.unshift(function(){delete T.style}),C.testAllProps=h;var E=C.prefixed=function(e,n,t){return 0===e.indexOf("@")?b(e):(-1!=e.indexOf("-")&&(e=s(e)),n?h(e,n,t):h(e,"pfx"))};Modernizr.addTest("objectfit",!!E("objectFit"),{aliases:["object-fit"]});var P=C._config.usePrefixes?" -webkit- -moz- -o- -ms- ".split(" "):["",""];C._prefixes=P;var N=C.testStyles=d;Modernizr.addTest("touchevents",function(){var t;if("ontouchstart"in e||e.DocumentTouch&&n instanceof DocumentTouch)t=!0;else{var r=["@media (",P.join("touch-enabled),("),"heartz",")","{#modernizr{top:9px;position:absolute}}"].join("");N(r,function(e){t=9===e.offsetTop})}return t}),o(),i(y),delete C.addTest,delete C.addAsyncTest;for(var k=0;k<Modernizr._q.length;k++)Modernizr._q[k]();e.Modernizr=Modernizr}(window,document);
    </script>

    <?php wp_head(); ?>
    
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-25733066-1', 'auto');
    ga('send', 'pageview');
    </script>

    <!-- Hotjar Tracking Code for http://senseadvertising.com.au -->
    <script>
       (function(h,o,t,j,a,r){
           h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
           h._hjSettings={hjid:251170,hjsv:5};
           a=o.getElementsByTagName('head')[0];
           r=o.createElement('script');r.async=1;
           r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
           a.appendChild(r);
       })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '935565863238433');
    fbq('track', "PageView");
    fbq('track', 'ViewContent');
    fbq('track', 'Lead');</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=935565863238433&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

</head>

<body <?php body_class(); ?>>

    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NT58XK"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NT58XK');</script>
    <!-- End Google Tag Manager -->

    <header id="header" class="header">

        <div class="header__controls">
            <a href="<?php bloginfo('url'); ?>" class="header__logo">
                <?php echo file_get_contents(get_template_directory()."/img/logo.svg"); ?>
            </a>

            <button class="toggle js-menu-toggle" data-menu=".header">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </div>

        <div class="header__inner">

            <div class="header__panel header__inner_panel">

                <?php wp_nav_menu(array(
                    'menu'              => 'header',
                    'theme_location'    => 'header',
                    'depth'             => 0,
                    'container'         => '',
                    'container_class'   => '',
                    'menu_class'        => 'header__menu',
                    'fallback_cb'       => false
                )); ?>

                <div class="header__footer">
                    <p>Call Sense on <a href="tel:+61 3 8456 7519" class="color--pink">+61 3 8456 7519</a></p>
                    <ul class="social">
                        <li>
                            <a class="btn--social socicon-facebook" href="https://www.facebook.com/senseagency" target="_blank">
                                <span class="hover_line hover_line--small hover_line--white"></span>
                            </a>
                        </li>
                        <li>
                            <a class="btn--social socicon-twitter" href="https://twitter.com/sense_agency" target="_blank">
                                <span class="hover_line hover_line--small hover_line--white"></span>
                            </a>
                        </li>
                        <li class="alt">
                            <a class="btn--social socicon-instagram" href="https://www.instagram.com/sense.agency/" target="_blank">
                                <span class="hover_line hover_line--small hover_line--white"></span>
                            </a>
                        </li>
                        <li class="alt">
                            <a class="btn--social socicon-linkedin" href="https://www.facebook.com/senseagency" target="_blank">
                                <span class="hover_line hover_line--small hover_line--white"></span>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>

        </div>

    </header>

    <div class="scroll-fix primary_content">
