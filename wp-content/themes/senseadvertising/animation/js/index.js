jQuery(document).ready(function(){

    var scrollimateables = [],
        $window = $(window),
        $document = $(document);

    $(".scrollimate").each(function(key, element){
        var path_length = element.getTotalLength();

        scrollimateables.push({
            element: element,
            path_length: path_length
        });

        element.style.strokeDasharray =  '0 ' + path_length;
        element.style.strokeDashoffset = -path_length/2;
        element.getBoundingClientRect();

    });

    $window.scroll(function(){
        var scrollPercentage = $window.scrollTop() / ($document.height()-$window.height());
        $.each(scrollimateables,function(key,scrollimate){
            var drawLength = scrollimate.path_length * scrollPercentage;// Length to offset the dashes
            scrollimate.element.style.strokeDashoffset = -scrollimate.path_length/2 + (drawLength/2);
            scrollimate.element.style.strokeDasharray  = drawLength + " " + scrollimate.path_length;
        });
    });
});