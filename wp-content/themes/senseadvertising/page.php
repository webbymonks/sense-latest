<?php get_header(); ?>

<div class="container-fluid">
    <?php while(have_posts()) { the_post();
        get_template_part( 'content', 'page' );
    } ?>
</div>

<?php get_footer();