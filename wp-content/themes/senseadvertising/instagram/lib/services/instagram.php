<?php
// ----------------------------------------------------------------
// https://www.instagram.com/developer/authentication/
// https://www.instagram.com/developer/endpoints/
// ----------------------------------------------------------------

class InstagramService extends Service {

    protected $credentials = array(
        'clientID' => array(
            'name' => 'Client ID',
            'value' => '236014c3103144f89ec963c7bdddba6d',
            'disabled' => false
        ),
        'clientSecret' => array(
            'name' => 'Client Secret',
            'value' => '57d79fc85dd748ac8b51e63962388a77',
            'disabled' => false
        )
    );

	public function __construct(){
        if(!$this->getAccesKey() && is_user_logged_in()){
            $this->authenticate();
        }
	}

    public function get_feed(){
        if($this->getAccesKey()){
            return $this->makeRequest();
        }else{
            return false;
        }

    }

    private function authenticate(){

        if($this->getAccesKey()){

        }elseif(isset($_GET['service']) && $_GET['service']=='instagram' && isset($_GET['code'])){
            $response = $this->curlPost('https://api.instagram.com/oauth/access_token', array(
                'client_id'     => $this->credentials['clientID']['value'],
                'client_secret' => $this->credentials['clientSecret']['value'],
                'redirect_uri'  => $this->getRedirectURL('instagram'),
                'grant_type'    => 'authorization_code',
                'code'  		=> $_GET['code']
            ));

            // ----------------------------------------------------------------
            // Check the response
            // ----------------------------------------------------------------
            if(property_exists($response, 'access_token')){
                print_r($response);
            }else{
                $this->saveAccessKey($response->access_token);
            }
        }else{
            $authURL = "https://api.instagram.com/oauth/authorize/?client_id=".$this->credentials['clientID']['value']."&redirect_uri=".$this->getRedirectURL('instagram')."&response_type=token";
            echo "<a href='$authURL'>Instagram</a>";
        }
    }

    private function makeRequest(){
        // $id = $_SESSION['instagram']['auth_user']->id;
        // $url = "https://api.instagram.com/v1/users/$id/media/recent/";
        // delete_transient('instagram_data');

        if($data = get_transient('instagram_data')){
            return $data;
        }

        $url = 'https://api.instagram.com/v1/users/self/media/recent';
        $fields = array(
            'access_token'  => $this->getAccesKey(),
            'count'         => 3
        );
        $response = $this->curlGet($url, $fields);
        set_transient('instagram_data', $response, 24 * HOUR_IN_SECONDS);
        return $response;
    }

    private function saveAccessKey($token){
        update_option('instagram_access_token', $token, false);
        // set_transient('instagram_access_token', $token,0);
    }
    private function getAccesKey(){
        return get_option('instagram_access_token');
        // return get_transient('instagram_access_token');
    }
}
