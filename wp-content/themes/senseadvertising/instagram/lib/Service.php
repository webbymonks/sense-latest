<?php

class Service {
	public function __construct(){

	}

    protected function curlGet($url, $parameters){
        return $this->curl($url, $parameters);
    }

    protected function curlPost($url, $parameters){
        return $this->curl($url, $parameters, false);
    }

    private function curl($url, $parameters, $get = true){
        // ----------------------------------------------------------------
        // Create a curl object
        // ----------------------------------------------------------------
        $ch = curl_init();

        // ----------------------------------------------------------------
        // Put together the curl request
        // ----------------------------------------------------------------
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,    false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,    30);

        if($get===true){
            curl_setopt($ch, CURLOPT_URL, $url . '?' . $this->arrayToPost($parameters));
        }else{
            curl_setopt($ch, CURLOPT_URL,               $url);
            curl_setopt($ch, CURLOPT_POST,              count($parameters));
            curl_setopt($ch, CURLOPT_POSTFIELDS,        $this->arrayToPost($parameters));
        }

        // ----------------------------------------------------------------
        // Make the curl request
        // ----------------------------------------------------------------
        $response = json_decode(curl_exec($ch));

        // ----------------------------------------------------------------
        // Close the request
        // ----------------------------------------------------------------
        curl_close($ch);

        return $response;
    }

    private function arrayToPost($array){
        $string = '';

        $index = 0;
        foreach($array as $key=> $value) {

            if($index!=0){
                $string .='&';
            }

            $string .= $key.'='.$value;
            $index++;
        }
        return $string;
    }

    public function getCredentials(){
        return $this->credentials;
    }

    public function restoreCredentials($credentials){
        foreach ($credentials as $key => $credential) {
            $this->credentials[$key]['value'] = $credential;
        }
    }

    protected function getRedirectUrl($service){
        return $this->getBaseUrl() . '?service=' . $service;
    }

    protected static function getBaseUrl(){
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
        return $protocol . $_SERVER['HTTP_HOST'] . strtok($_SERVER["REQUEST_URI"],'?');
    }
}
